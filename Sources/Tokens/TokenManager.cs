﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace Iodynis.Libraries.Parsing
{
    /// <summary>
    /// Token manager handles the token graph and provides methods to manipulate it.
    /// </summary>
    public class TokenManager
    {
        #region Fields

        //
        // Token tree structure by the example of a single token connections:
        //
        //                               ┌───────┐
        //                               │ Token │                                  Parent.
        //                               └───────┘                                    Ordinary token or root or zero.
        //                                   A
        //                                   │
        //                                up │
        //                                   │
        //                                   │
        //         ┌───────┐     left    ╔═══════╗    right    ┌───────┐
        //         │ Token │ <────────── ║ Token ║ ──────────> │ Token │            Neighbours.
        //         └───────┘             ╚═══════╝             └───────┘              Ordinary tokens and/or zeros.
        //                                  │ │
        //                           first  │ │ last
        //                                  │ │
        //             ┌────────────────────┘ └────────────────────┐
        //             │                                           │
        //             V                                           V
        //         ┌───────┐   ┌───────┐           ┌───────┐   ┌───────┐
        //         │ Token │   │ Token │    ...    │ Token │   │ Token │            Children.
        //         └───────┘   └───────┘           └───────┘   └───────┘              Ordinary tokens and/or zeros.
        //
        //       └────────────────────────────────────────────────────────┘
        //                                 count
        //


        private readonly List<int> Roots = new List<int>();
        private int Capacity = 0;
        private int Counter = 1 /* 0th element is the zero */;
        private ulong[] Types;
        private string[] Symbols;
        private int[] TextStarts;
        private int[] TextEnds;
        private object[] Objects;

        // Vertical links
        private int[] Ups;
        private int[] Firsts;
        private int[] Lasts;

        // Horizontal links
        private int[] Lefts;
        private int[] Rights;
        //private int[] firsts; <-- these are duplicates for downs[ups[...]]. The only case when this substitution doesn't work is for roots but we don't need it, do we?..
        private int[] Counts;

        // Indexes for last passes performed on the tokens
        // This will prevent processing tokens multiple times on the same pass
        private int Pass = 0;
        private int[] Passes;

        // Recycling
        private int[] Trash;
        private int TrashCounter = 0;

        // New lines
        private Dictionary<int, SortedList<int, int>> RootToNewLinePositionToNewLineNumer = new Dictionary<int, SortedList<int, int>>();

        public List<Token> UnknownTokens = new List<Token>();

        private StringBuilder StringBuilder = new StringBuilder();
        #endregion

        #region Constructors
        /// <summary>
        /// ... with default capacity for 65536 tokens
        /// </summary>
        public TokenManager()
            : this(1024 * 64 /* This should allocate ~ 1MB of memory what seems to be a reasonable amount */) { }
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="capacity">Initial capacity of the token graph. Will expand automatically by factor of 2 if needed. Capacity lower bound is set to 1024.</param>
        public TokenManager(int capacity)
        {
            Capacity = capacity;

            // Capacity cannot be lower than 2 -- the zero and the root elements are always presented
            // But there is no sense in allocating that small amount of memory, start with a bit more reasonable value
            if (Capacity < 1024)
            {
                Capacity = 1024;
            }

            Types      = new ulong[Capacity];
            Symbols    = new string[Capacity];
            TextStarts = new int[Capacity];
            TextEnds   = new int[Capacity];
            Objects    = new object[Capacity];
            Lefts      = new int[Capacity];
            Rights     = new int[Capacity];
            Ups        = new int[Capacity];
            Firsts     = new int[Capacity];
            Lasts      = new int[Capacity];
            Counts     = new int[Capacity];

            Passes     = new int[Capacity];
            Trash      = new int[Capacity];

            //symbols[0] = "[ZERO]";
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Reset(int capacity)
        {
            // If current capacity is enough
            if (capacity <= this.Capacity)
            {
                Reset();
                return;
            }

            // Expand
            Capacity = capacity;
            // Allocate memory
            Types      = new ulong[capacity];
            Symbols    = new string[capacity];
            TextStarts = new int[capacity];
            TextEnds   = new int[capacity];
            Objects    = new object[capacity];
            Lefts      = new int[capacity];
            Rights     = new int[capacity];
            Ups        = new int[capacity];
            Firsts     = new int[capacity];
            Lasts      = new int[capacity];
            Counts     = new int[capacity];
            Passes     = new int[capacity];
            // Reset
            Counter = 1 /* 0th element is the zero */;
            TrashCounter = 0;
            Roots.Clear();
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Reset()
        {
            // Zero the first element
            Types[1] = 0;
            Symbols[1] = null;
            TextStarts[1] = 0;
            TextEnds[1] = 0;
            Objects[1] = null;
            Lefts[1] = 0;
            Rights[1] = 0;
            Ups[1] = 0;
            Firsts[1] = 0;
            Lasts[1] = 0;
            Counts[1] = 0;
            Passes[1] = 0;
            // Reset
            Counter = 1 /* 0th element is the zero */;
            TrashCounter = 0;
            Roots.Clear();
        }
        //[MethodImpl(MethodImplOptions.AggressiveInlining)]
        //public void Set(string symbol)
        //{
        //    Reset();
        //    AddRoot(symbol);
        //}
        #endregion

        #region Navigate
        /// <summary>
        /// Get the token to the left.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>Token to the left.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int GetLeft(int token)
        {
            return Lefts[token];
        }
        /// <summary>
        /// Get the n-th token to the left of the specified token.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <param name="count">N.</param>
        /// <returns>N-th token to the left.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int GetLeft(int token, int count)
        {
            while (count-- > 0)
            {
                token = Lefts[token];
            }
            return token;
        }
        /// <summary>
        /// Get the token to the right.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>Token to the right.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int GetRight(int token)
        {
            return Rights[token];
        }
        /// <summary>
        /// Get the n-th token to the right of the specified token.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <param name="count">N.</param>
        /// <returns>N-th token to the right.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int GetRight(int token, int count)
        {
            while (count-- > 0)
            {
                token = Rights[token];
            }
            return token;
        }
        /// <summary>
        /// Get the parent token.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>Parent token.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int GetUp(int token)
        {
            return Ups[token];
        }
        /// <summary>
        /// Get the first child.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>First child token.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int GetDownFirst(int token)
        {
            return Firsts[token];
        }
        /// <summary>
        /// Get the first child.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>First child token.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int GetDownLast(int token)
        {
            return Lasts[token];
            //int last = firsts[token];
            //while (rights[last] != 0)
            //{
            //    last = rights[last];
            //}
            //return last;
        }
        /// <summary>
        /// Get the leftmost (first in the line) token relatively to the specified token.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>The leftmost token in the line.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int GetFirst(int token)
        {
            if (token == 0)
            {
                return 0;
            }
            // The root element is the only one in the row
            if (Ups[token] == 0)
            {
                return token;
            }
            return Firsts[Ups[token]];
        }
        /// <summary>
        /// Get the rightmost (last in the line) token relatively to the specified token.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>The rightmost token in the line.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int GetLast(int token)
        {
            if (token == 0)
            {
                return 0;
            }
            // The root element is the only one in the row
            if (Ups[token] == 0)
            {
                return token;
            }
            return Lasts[Ups[token]];
            //while (rights[token] != 0)
            //{
            //    token = rights[token];
            //}
            //return token;
            ////return lasts[token];
        }
        /// <summary>
        /// Get count of tokens in the line.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>Tokens in the line count.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int GetCount(int token)
        {
            if (token == 0)
            {
                return 0;
            }
            // The root element is the only one in the row
            if (Ups[token] == 0)
            {
                return 1;
            }
            return Counts[Ups[token]];
        }
        /// <summary>
        /// Get count of children of the token.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>Children count.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int GetDownCount(int token)
        {
            return Counts[token];
        }
        /// <summary>
        /// Get the root of the specified token.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>Root token.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int GetRoot(int token)
        {
            while (Ups[token] != 0)
            {
                token = Ups[token];
            }
            return token;
        }
        /// <summary>
        /// Get all roots.
        /// </summary>
        /// <returns>A copy of the internal list of all roots.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public List<int> GetRoots()
        {
            return new List<int>(Roots);
        }
        /// <summary>
        /// Get next token in top-to-bottom direction.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int GetNext(int token, bool down = true)
        {
            // Try to go down
            if (down && Firsts[token] != 0)
            {
                return Firsts[token];
            }

            // Try to go right
            if (Rights[token] != 0)
            {
                return Rights[token];
            }

            // Try to go up and right
            int up = Ups[token];
            while (up != 0)
            {
                if (Rights[up] != 0)
                {
                    return Rights[up];
                }
                up = Ups[up];
            }

            // Got to the root
            return 0;
        }
        /// <summary>
        /// Get the first token to use when walking the token tree bottom-to-top.
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int GetDeeplyFirst(int token)
        {
            if (Firsts[token] != 0)
            {
                while (Firsts[token] != 0)
                {
                    token = Firsts[token];
                }
            }
            return token;
        }
        /// <summary>
        /// Get next token in bottom-to-top direction.
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int GetNextReversed(int token)
        {
            // Go right
            if (Rights[token] != 0)
            {
                token = Rights[token];
                // Drill down
                while (Firsts[token] != 0)
                {
                    token = Firsts[token];
                }
                return token;
            }
            // Go up
            if (Ups[token] != 0)
            {
                return Ups[token];
            }

            // Got to the root
            return 0;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int GetPrevious(int token)
        {
            // Try to go left
            if (Lefts[token] != 0)
            {
                return Lefts[token];
            }

            // Try to go up
            if (Ups[token] != 0)
            {
                return Ups[token];
            }

            // Got to the root
            return 0;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int GetNextUnpassed(int token, bool down = true)
        {
            int tokenStart = token;
            while (Passes[token] >= Pass)
            {
                token = GetNext(token, down);
                // Check for a loop
                if (token == tokenStart)
                {
                    List<int> tokens = new List<int>() { tokenStart };
                    token = GetNext(token, down);
                    while (token != tokenStart)
                    {
                        tokens.Add(token);
                        token = GetNext(token, down);
                    }
                    string sequence = String.Join(" ", tokens.Select(_token => Symbols[_token]));
                    int start = tokens.Min(_token => TextStarts[_token]);
                    int end = tokens.Max(_token => TextEnds[_token]);
                    string substring = Symbols[GetRoot(tokenStart)].Substring(start, end - start);// + 1);
                    throw new Exception($"Loop detected: {sequence}");
                }
            }
            return token;
        }
        #endregion

        #region Modify
        //public Token GetToken(Type type, int token)
        //{
        //    //return new Token((dynamic)Enum.ToObject(type, token), symbols[token], textStarts[token], textEnds[token]);
        //    return new Token(token, symbols[token], textStarts[token], textEnds[token]);
        //}
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Set(int token, ulong type, string symbol)
        {
            Symbols[token] = symbol;
            Types[token] = type;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Set(int token, ulong type, string symbol, int textStart, int textEnd)
        {
            Symbols[token] = symbol;
            Types[token] = type;
            TextStarts[token] = textStart;
            TextEnds[token] = textEnd;
        }
        /// <summary>
        /// Get token type.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>The token type.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public ulong GetType(int token)
        {
            return Types[token];
        }
        /// <summary>
        /// Set token type.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <param name="type">The token type.</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void SetType(int token, ulong type)
        {
            Types[token] = type;
        }
        /// <summary>
        /// Get token symbol.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>Token symbol.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public string GetSymbol(int token)
        {
            return Symbols[token];
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void SetSymbol(int token, string symbol)
        {
            Symbols[token] = symbol;
        }
        /// <summary>
        /// Get index of the start of the token in the text.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>Index of the start of the token in the text.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int GetTextStart(int token)
        {
            return TextStarts[token];
        }
        /// <summary>
        /// Set the index of the start of the token in the text.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <param name="textStart">Index of the start of the token in the text.</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void SetTextStart(int token, int textStart)
        {
            TextStarts[token] = textStart;
        }
        /// <summary>
        /// Get index of the end of the token in the text.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>Index of the end the token in the text.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int GetTextEnd(int token)
        {
            return TextEnds[token];
        }
        /// <summary>
        /// Set the index of the end of the token in the text.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <param name="textEnd">Index of the end of the token in the text.</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void SetTextEnd(int token, int textEnd)
        {
            TextEnds[token] = textEnd;
        }
        /// <summary>
        /// Get text fragment that corresponds to the token.
        /// This may differ from the Symbol and include more characters to the left and/or right.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>The corresponding text fragment.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public string GetText(int token)
        {
            int root = GetRoot(token);
            if (TextStarts[token] >= Symbols[root].Length)
            {
                return null;
            }
            if (TextEnds[token] > Symbols[root].Length)
            {
                return null;
            }
            return Symbols[root].Substring(TextStarts[token], TextEnds[token] - TextStarts[token]);
        }
        /// <summary>
        /// Set the index of the start of the token in the text.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <param name="textStart">Index of the start of the token in the text.</param>
        /// <param name="textEnd">Index of the end of the token in the text.</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void SetText(int token, int textStart, int textEnd)
        {
            TextStarts[token] = textStart;
            TextEnds[token] = textEnd;
        }
        /// <summary>
        /// Get the corresponding object, attached to the token.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>The corresponding object.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public object GetObject(int token)
        {
            return Objects[token];
        }
        /// <summary>
        /// Get the corresponding objects of child tokens attached to them.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>The corresponding objects of child tokens.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public object[] GetObjects(int token)
        {
            if (Counts[token] == 0)
            {
                return new object[0];
            }
            object[] objects = new object[Counts[token]];

            int child = Firsts[token];
            for (int index = 0; index < Counts[token]; index++)
            {
                objects[index] = this.Objects[child];
                child = Rights[child];
            }
            return objects;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void SetObject(int token, object @object)
        {
            Objects[token] = @object;
        }
        /// <summary>
        /// Starts a new pass.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void StartPass()
        {
            Pass++;
        }
        /// <summary>
        /// Marks specified token as already passed on the current pass.
        /// </summary>
        /// <param name="token">The token.</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void SetPassed(int token)
        {
            Passes[token] = Pass;
        }
        #endregion

        #region Add / remove
        /// <summary>
        /// Add a new root.
        /// </summary>
        /// <param name="symbol">Root symbol.</param>
        /// <returns>The root token.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int AddRoot(string symbol)
        {
            // Add a new token
            int root = Create((int)TokenBasic.ROOT, symbol, 0, symbol.Length);
            Roots.Add(root);

            // Scan the new line symbols to report any problems using line number and position index
            ScanNewLines(root);

            // Chain it with other roots
            if (Roots.Count > 1)
            {
                int rootPrevious = Roots[Roots.Count - 2];
                Lefts[root] = rootPrevious;
                Rights[rootPrevious] = root;
            }

            return root;
        }
        /// <summary>
        /// Scans for new lines in the text and saves their indexes.
        /// Call to this is mandatory for GetLineAndPosition to work properly.
        /// </summary>
        /// <param name="root">The root token.</param>
        private void ScanNewLines(int root)
        {
            SortedList<int, int> newLinePositions = new SortedList<int, int>() { { 0, 0 } };
            string symbol = GetSymbol(root);
            for (int symbolIndex = 0; symbolIndex < symbol.Length; symbolIndex++)
            {
                if (symbol[symbolIndex] == '\n')
                {
                    newLinePositions.Add(symbolIndex, newLinePositions.Count);// + 1 /* we need the index of the new new-line */);
                }
            }
            RootToNewLinePositionToNewLineNumer[root] = newLinePositions;
        }
        /// <summary>
        /// Get line number and position of the start of the token in the original text.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>Line number and position in the line.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public (int Line, int Position) GetLineAndPosition(int token)
        {
            // Absolute position in the whole text
            int tokenPosition = GetTextStart(token);
            // Root token
            int root = GetRoot(token);
            // New lines
            SortedList<int, int> newLinePositionToNewLineNumber = RootToNewLinePositionToNewLineNumer[root];

            int newLinePosition = GetNearestDown(newLinePositionToNewLineNumber, tokenPosition);
            int newLineNumber = newLinePositionToNewLineNumber[newLinePosition] + 1 /* line number starts from 1 instead of 0 */;

            // Corresponding line number
            int lineNumber = newLineNumber;
            // Position in the corresponding line
            int positionIndex = tokenPosition - newLinePosition;

            return (lineNumber, positionIndex);
        }
        /// <summary>
        /// Get line number and line position of the specified position in the original text of the root of the specified token.
        /// </summary>
        /// <param name="token">Root token which text to use for the navigation. Automatic ascent to the root token will be done if the provided token is not a root token.</param>
        /// <param name="absolutePositionInText">Absolute position in the text of the root token.</param>
        /// <returns>Line number and position in the line.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public (int Line, int Position) GetLineAndPosition(int token, int absolutePositionInText)
        {
            // Root token
            token = GetRoot(token);
            // New lines
            SortedList<int, int> newLinePositionToNewLineNumber = RootToNewLinePositionToNewLineNumer[token];

            int newLinePosition = GetNearestDown(newLinePositionToNewLineNumber, absolutePositionInText);
            int newLineNumber = newLinePositionToNewLineNumber[newLinePosition] + 1 /* line number starts from 1 instead of 0 */;

            // Corresponding line number
            int lineNumber = newLineNumber;
            // Position in the corresponding line
            int positionIndex = absolutePositionInText - newLinePosition;

            return (lineNumber, positionIndex);
        }
        /// <summary>
        /// Add a new token.
        /// </summary>
        /// <param name="type">The token type.</param>
        /// <param name="symbol">The token symbol.</param>
        /// <param name="textStart">Index in the original text (symbol of the root) at which the token starts.</param>
        /// <param name="textEnd">Index in the original text (symbol of the root) at which the token ends.</param>
        /// <returns>Token.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int Create(ulong type, string symbol, int textStart, int textEnd)
        {
            int token;
            // Check if can reuse memory
            if (TrashCounter > 0)
            {
                token = Trash[--TrashCounter];

                // Zero
                Lefts[token] = 0;
                Rights[token] = 0;
                Ups[token] = 0;
                Firsts[token] = 0;
                Lasts[token] = 0;
                Counts[token] = 0;
                Objects[token] = null;
            }
            else
            {
                // Ensure there is enough space
                while (Counter >= Capacity)
                {
                    Expand();
                }
                token = Counter++;
            }

            Types[token] = type;
            Symbols[token] = symbol;
            TextStarts[token] = textStart;
            TextEnds[token] = textEnd;

            return token;
        }
        /// <summary>
        /// Duplicate an existing token.
        /// </summary>
        /// <param name="token">The token to duplicate.</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int Duplicate(int token)
        {
            return Create(Types[token], Symbols[token], TextStarts[token], TextEnds[token]);
        }
        /// <summary>
        /// Cut tokens and their children out from the token graph and mark all of them for recycling.
        /// </summary>
        /// <param name="tokens">The tokens to delete.</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Delete(params int[] tokens)
        {
            for (int tokenIndex = 0; tokenIndex < tokens.Length; tokenIndex++)
            {
                Delete(tokens[tokenIndex]);
            }
        }
        /// <summary>
        /// Cut token and its children out from the token graph and mark all of them for recycling.
        /// </summary>
        /// <param name="tokens">The token to delete.</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Delete(int token)
        {
            DeleteChildren(token);

            // Cut the token from the tree
            Cut(token);

            // Add to the recycling pool
            Recycle(token);
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void DeleteBlock(int left, int right)
        {
            // If just one token
            if (left == right)
            {
                Delete(left);
                return;
            }

            int current = left;
            int next = Rights[left];
            do
            {
                if (current == 0)
                {
                    throw new Exception("Got out of bounds while deleting a block.");
                }
                Delete(current);
                current = next;
                next = Rights[next];
            } while (current != right);
        }
        /// <summary>
        /// Mark tokens for recycling. This does not modify the tokens or their connections to other tokens or connections of other tokens to themselves.
        /// </summary>
        /// <param name="tokens">The tokens to mark for recycling.</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Recycle(params int[] tokens)
        {
            for (int tokenIndex = 0; tokenIndex < tokens.Length; tokenIndex++)
            {
                Recycle(tokens[tokenIndex]);
            }
        }
        /// <summary>
        /// Mark token for recycling. This does not modify the token or its connections to other tokens or connections of other tokens to itself.
        /// </summary>
        /// <param name="token">The token to mark for recycling.</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Recycle(int token)
        {
            // Expand the trash if neccessary
            if (TrashCounter >= Trash.Length)
            {
                ExpandTrash();
            }
            Trash[TrashCounter++] = token;
        }
        /// <summary>
        /// Cut tokens out from the token graph.
        /// </summary>
        /// <param name="tokens">The tokens to cut out.</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Cut(params int[] tokens)
        {
            for (int tokenIndex = 0; tokenIndex < tokens.Length; tokenIndex++)
            {
                Cut(tokens[tokenIndex]);
            }
        }
        /// <summary>
        /// Cut token out from the token graph.
        /// </summary>
        /// <param name="token">The token to cut out.</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Cut(int token)
        {
            int left = Lefts[token];
            int right = Rights[token];

            // Modify neighbours
            if (left != 0)
            {
                Rights[left] = right;
            }
            if (right != 0)
            {
                Lefts[right] = left;
            }
            // Modify parent
            if (Ups[token] != 0)
            {
                if (Firsts[Ups[token]] == token)
                {
                    if (right != 0)
                    {
                        Firsts[Ups[token]] = right;
                    }
                    else
                    {
                        Firsts[Ups[token]] = 0;
                    }
                }
                if (Lasts[Ups[token]] == token)
                {
                    if (left != 0)
                    {
                        Lasts[Ups[token]] = left;
                    }
                    else
                    {
                        Lasts[Ups[token]] = 0;
                    }
                }
                Counts[Ups[token]]--;
            }
        }
        //[MethodImpl(MethodImplOptions.AggressiveInlining)]
        //public int Remove(int token)
        //{
        //    //if (trashCounter >= trash.Length)
        //    //{
        //    //    ExpandTrash();
        //    //}
        //    //trash[trashCounter++] = token;

        //    //types[token] = -1;
        //    //#if DEBUG
        //    //symbols[token] = "REMOVED " + symbols[token];
        //    //#endif

        //    int next = 0;
        //    // Left and right
        //    {
        //        if (lefts[token] != 0)
        //        {
        //            next = lefts[token];
        //            rights[lefts[token]] = rights[token];
        //        }
        //        else if (ups[token] != 0)
        //        {
        //            next = ups[token];
        //            downs[ups[token]] = rights[token]; // Point the parent to the next token in the row
        //        }
        //        if (rights[token] != 0)
        //        {
        //            if (next == 0)
        //            {
        //                next = rights[token];
        //            }
        //            lefts[rights[token]] = lefts[token];
        //        }
        //    }
        //    return next;

        //    //// Down
        //    //{
        //    //    if (downs[token] != 0)
        //    //    {
        //    //        // Actually, nothing needs to be removed as nothing now links down there.
        //    //        // Yes, it occupies memory, but moving stuff would be much of a hassle.
        //    //    }
        //    //}

        //    //// Up
        //    //{
        //    //    if (ups[token] != 0 /* there is a parent */ && downs[ups[token]] == token /* the parent points down to this exact token */)
        //    //    {
        //    //        if (next == 0)
        //    //        {
        //    //            next = ups[token];
        //    //        }
        //    //        downs[ups[token]] = rights[token]; // Point the parent to the next token in the row
        //    //    }
        //    //}
        //}
        #endregion

        #region Manipulate
        /// <summary>
        /// Move all children from one token to another. If the destination token already has some children, then the new ones are appended to them from the right.
        /// </summary>
        /// <param name="tokenTo">The token to move children from.</param>
        /// <param name="tokenFrom">The token to move children to.</param>
        public void MoveChildren(int tokenTo, int tokenFrom)
        {
            int childFromFirst = Firsts[tokenFrom];
            if (childFromFirst == 0)
            {
                return;
            }
            // Interconnect last child and first child
            int childToLast = Lasts[tokenTo];
            if (childToLast != 0)
            {
                Rights[childToLast] = childFromFirst;
                Lefts[childFromFirst] = childToLast;
            }
            // Or update the first child
            else
            {
                Firsts[tokenTo] = childFromFirst;
            }
            // Redirect to the new parent
            int child = childFromFirst;
            while (child != 0)
            {
                Ups[child] = tokenTo;
                child = Rights[child];
            }

            // Update children counts
            Counts[tokenTo] += Counts[tokenFrom];
            Counts[tokenFrom] = 0;
            // Update the last child
            Lasts[tokenTo] = Lasts[tokenFrom];
            // Remove direction to the child from now empty parent
            Firsts[tokenFrom] = 0;
            Lasts[tokenFrom] = 0;
        }
        /// <summary>
        /// Delete all children of the token.
        /// </summary>
        /// <param name="token">The token.</param>
        public void DeleteChildren(int token)
        {
            // Trash all children
            int child = Firsts[token];
            while (child != 0)
            {
                // Recursive call
                DeleteChildren(child);
                // Trash the child
                Recycle(child);
                // Move to next
                child = Rights[child];
            }
            // Reset
            Firsts[token] = 0;
            Lasts[token] = 0;
            Counts[token] = 0;
        }
        /// <summary>
        /// Delete all children token of specified type.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <param name="type">The token type to delete.</param>
        public void DeleteChildrenOfType(int token, ulong type)
        {
            int child = Firsts[token];

            // Trash all children of specified type
            while (child != 0)
            {
                if (Types[child] == type)
                {
                    int right = Rights[child];
                    Delete(child);
                    child = right;
                }
            }
        }
        /// <summary>
        /// Replace the token with its children. If the token has no children it will be deleted.
        /// </summary>
        /// <param name="token">The token.</param>
        public void ReplaceWithChildren(int token)
        {
            if (Firsts[token] == 0)
            {
                Delete(token);
                return;
            }

            int childFirst = Firsts[token];
            int childLast = Lasts[token];

            int child = childFirst;
            // Up
            while (child != 0)
            {
                Ups[child] = Ups[token];
                child = Rights[child];
            }
            // Left and right
            Lefts[childFirst] = Lefts[token];
            Rights[childLast] = Rights[token];
            Rights[Lefts[token]] = childFirst;
            Lefts[Rights[token]] = childLast;
            // Down
            if (Firsts[Ups[token]] == token)
            {
                Firsts[Ups[token]] = childFirst;
            }
            if (Lasts[Ups[token]] == token)
            {
                Lasts[Ups[token]] = childLast;
            }
            Counts[Ups[token]] += Counts[token];
            // Mark token for deletion
            Recycle(token);
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int Replace(int tokenStart, int tokenEnd, ulong type)
        {
            return Replace(tokenStart, tokenEnd, type, null);
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int Replace(int tokenStart, int tokenEnd, ulong type, string symbol)
        {
            if (Rights[tokenEnd] == 0)
            {
                Rights[tokenStart] = 0;
                Lasts[Ups[tokenStart]] = tokenStart;
            }
            else
            {
                Rights[tokenStart] = Rights[tokenEnd];
                Lefts[Rights[tokenEnd]] = tokenStart;
            }

            Firsts[tokenStart] = 0;
            Lasts[tokenStart] = 0;

            Types[tokenStart] = type;
            Symbols[tokenStart] = symbol;
            return tokenStart;
        }
        /// <summary>
        /// Split children tokens into groups using delimiter provided.
        /// </summary>
        /// <param name="parentToken">The token whose children to split.</param>
        /// <param name="groupType">The group token type to use when grouping the children.</param>
        /// <param name="delimiterType">The delimiter token type to search for as the separator.</param>
        /// <param name="includeEmptyGroups">Whether to include empty groups or not.</param>
        public void SplitChildren(int parentToken, ulong groupType, ulong delimiterType, bool includeEmptyGroups = true)
        {
            Split(Firsts[parentToken], true, 0, true, groupType, null, delimiterType, includeEmptyGroups);
        }
        /// <summary>
        /// Split children tokens into groups using delimiter provided.
        /// </summary>
        /// <param name="parentToken">The token whose children to split.</param>
        /// <param name="groupType">The group token type to use when grouping the children.</param>
        /// <param name="groupSymbol">The group token symbol to use.</param>
        /// <param name="delimiterType">The delimiter token type to search for as the separator.</param>
        /// <param name="includeEmptyGroups">Whether to include empty groups or not.</param>
        public void SplitChildren(int parentToken, ulong groupType, string groupSymbol, ulong delimiterType, bool includeEmptyGroups = true)
        {
            Split(Firsts[parentToken], true, Lasts[parentToken], true, groupType, groupSymbol, delimiterType, includeEmptyGroups);
        }
        public int Split(int tokenStart, ulong groupType, ulong delimiterType, bool includeEmptyGroups = true)
        {
            return Split(tokenStart, true, 0, false, groupType, null, delimiterType, includeEmptyGroups);
        }
        public int Split(int tokenStart, ulong groupType, string groupSymbol, ulong delimiterType, bool includeEmptyGroups = true)
        {
            return Split(tokenStart, true, 0, false, groupType, groupSymbol, delimiterType, includeEmptyGroups);
        }
        public int Split(int tokenStart, int tokenEnd, ulong groupType, ulong delimiterType, bool includeEmptyGroups = true)
        {
            return Split(tokenStart, true, tokenEnd, true, groupType, null, delimiterType, includeEmptyGroups);
        }
        public int Split(int tokenStart, int tokenEnd, ulong groupType, string groupSymbol, ulong delimiterType, bool includeEmptyGroups = true)
        {
            return Split(tokenStart, true, tokenEnd, true, groupType, groupSymbol, delimiterType, includeEmptyGroups);
        }
        public int Split(int tokenStart, bool includeStart, int tokenEnd, bool includeEnd, ulong groupType, string groupSymbol, ulong delimiterType, bool includeEmptyGroups = true)
        {
            tokenEnd = tokenEnd == 0 ? Lasts[Ups[tokenStart]] : tokenEnd;
            if (tokenStart == tokenEnd)
            {
                if (!includeStart || !includeEnd)
                {
                    Delete(tokenStart);
                    return GetPrevious(tokenStart);
                }
                else
                {
                    return tokenStart;
                }
            }

            int tokenStartActual = includeStart ? tokenStart : Rights[tokenStart];
            int tokenEndActual = includeEnd ? tokenEnd : Lefts[tokenEnd];

            // No tokens in the group
            if (tokenStartActual == tokenEnd || tokenStart == tokenEndActual)
            {
                Delete(tokenStart);
                Delete(tokenEnd);
                return GetPrevious(tokenStart);
            }
            // Only one token is presented
            if (tokenStartActual == tokenEndActual)
            {
                // And it is a delimiter
                // Threat this as 2 empty groups
                if (Types[tokenStartActual] == delimiterType)
                {
                    SetType(tokenStartActual, groupType);
                    SetSymbol(tokenStartActual, groupSymbol);
                    DeleteChildren(tokenStartActual);
                    PasteRight(tokenStartActual, Duplicate(tokenStartActual));
                }
                else
                {
                    Group(groupType, groupSymbol, tokenStartActual, GroupMode.INCLUDE, tokenEndActual, GroupMode.INCLUDE);
                }
                return tokenStart;
            }

            //int tokenFirstGroup = 0;
            int tokenFirst = 0;
            int tokenPrevious = 0;
            int tokenCurrent = tokenStartActual;

            while (true)
            {
                // As actual bounds are determined at the start -- the 0 bound should not be hit
                if (tokenCurrent == 0)
                {
                    throw new Exception("Got out of bounds while splitting.");
                }

                // Delimiter
                if (Types[tokenCurrent] == delimiterType)
                {
                    // Group contains some tokens
                    if (tokenFirst != 0)
                    {
                        Group(groupType, groupSymbol, tokenFirst, GroupMode.INCLUDE, tokenCurrent, GroupMode.DELETE);
                        tokenFirst = 0;
                    }
                    // Group is empty
                    else
                    {
                        if (includeEmptyGroups)
                        {
                            // Change the existing delimiter token into a group token
                            Set(tokenCurrent, groupType, groupSymbol);
                            // Delete its children as it should be an empty group token now
                            DeleteChildren(tokenCurrent);
                        }
                        else
                        {
                            // Delete the token.
                            // Because the token is neither reset nor modified upon deletion the later call to rights[tokenCurrent] will return correct result.
                            Delete(tokenCurrent);
                        }
                    }
                    //tokenStartActual = rights[tokenCurrent];
                    //tokenFirst = 0;
                }
                // Not a delimiter
                else
                {
                    if (tokenFirst == 0)
                    {
                        tokenFirst = tokenCurrent;
                    }
                }

                // Got to the end
                if (tokenCurrent == tokenEndActual)
                {
                    // Delimiter
                    if (Types[tokenCurrent] == delimiterType)
                    {
                        if (includeEmptyGroups)
                        {
                            // There are no delimiters left, so instead of reusing them for group tokens add a new group token
                            int last = Lasts[tokenPrevious];
                            PasteRight(last, Create(groupType, groupSymbol, TextStarts[last], TextEnds[last]));
                        }
                    }
                    // Not a delimiter
                    else
                    {
                        Group(groupType, groupSymbol, tokenFirst != 0 ? tokenFirst : tokenCurrent, GroupMode.INCLUDE, tokenCurrent, GroupMode.INCLUDE);
                    }
                    return 0; // TODO: ACtual token of the first group
                }


                // Go to next token
                tokenPrevious = tokenCurrent;
                tokenCurrent  = Rights[tokenCurrent];

                //// End of sequence
                //if (tokenCurrent == tokenActualEnd)
                //{
                //    //// Group contains some tokens
                //    //if (tokenCurrent != tokenActualStart)
                //    //{
                //        Group(groupType, groupSymbol, tokenActualStart, true, tokenCurrent, false);
                //    //}
                //    //// Group is empty
                //    //else
                //    //{
                //    //    if (includeEmptyGroups)
                //    //    {
                //    //        // There are no delimiters left, so instead of reusing them for group tokens add a new group token
                //    //        int last = lasts[tokenPrevious];
                //    //        PasteRight(last, Create(groupType, groupSymbol, textStarts[last], textEnds[last]));
                //    //    }
                //    //    else
                //    //    {
                //    //        ;
                //    //    }
                //    //}
                //    return;
                //}

                //// End of sequence
                //if (tokenCurrent == tokenLast)
                //{
                //    // Group contains some tokens
                //    if (tokenCurrent != tokenFirst)
                //    {
                //        Group(groupType, groupSymbol, tokenFirst, true, tokenCurrent, true);
                //    }
                //    // Group is empty
                //    else
                //    {
                //        if (includeEmptyGroups)
                //        {
                //            // Change the existing delimiter token into a group token
                //            Set(tokenCurrent, groupType, groupSymbol);
                //            // Delete its children as it should be an empty group token now
                //            DeleteChildren(tokenCurrent);
                //        }
                //        else
                //        {
                //            // Delete the token.
                //            Delete(tokenCurrent);
                //        }
                //    }
                //    return;
                //}

                //// Delimiter
                //if (types[tokenCurrent] == delimiterType)
                //{
                //    // Group contains some tokens
                //    if (tokenCurrent != tokenActualStart)
                //    {
                //        tokenCurrent = Group(groupType, groupSymbol, tokenActualStart, true, tokenCurrent, false);
                //    }
                //    // Group is empty
                //    else
                //    {
                //        if (includeEmptyGroups)
                //        {
                //            // Change the existing delimiter token into a group token
                //            Set(tokenCurrent, groupType, groupSymbol);
                //            // Delete its children as it should be an empty group token now
                //            DeleteChildren(tokenCurrent);
                //        }
                //        else
                //        {
                //            // Delete the token.
                //            // Because the token is neither reset nor modified upon deletion the later call to rights[tokenCurrent] will return correct result.
                //            Delete(tokenCurrent);
                //        }
                //    }
                //    tokenActualStart = rights[tokenCurrent];
                //}

                //// Go to next token
                //tokenPrevious = tokenCurrent;
                //tokenCurrent  = rights[tokenCurrent];
            }
            //return ups[tokenStart];
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int GroupAround(int token, int tokensLeftCount, int tokensRightCount)
        {
            return GroupAround(0, null, token, tokensLeftCount, tokensRightCount);
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int GroupAround(ulong type, int token, int tokensLeftCount, int tokensRightCount)
        {
            return GroupAround(type, null, token, tokensLeftCount, tokensRightCount);
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int GroupAround(ulong type, string symbol, int token, int tokensLeftCount, int tokensRightCount)
        {
            // Get outermost tokens of the new group
            int left = GetLeft(token, tokensLeftCount);
            int right = GetRight(token, tokensRightCount);
            int parent = Ups[token];

            // Tokens will be grouped under the specified one
            int group = token;
            // Change type if specified
            if (type >= 0) Types[group] = type;
            // Change symbol if specified
            if (symbol != null) Symbols[group] = symbol;

            // Surrounding tokens
            if (Lefts[group] != 0)
            {
                Rights[Lefts[group]] = Rights[group];
            }
            if (Rights[group] != 0)
            {
                Lefts[Rights[group]] = Lefts[group];
            }

            // Group token
            Firsts[group] = left;
            Lasts[group] = right;
            Counts[group] = 1;
            Lefts[group] = Lefts[left];
            Rights[group] = Rights[right];
            if (Lefts[left] != 0) // If there are tokens to the left
            {
                Rights[Lefts[left]] = group;
            }
            else if (parent != 0) // If the left token is the first one and it has a parent
            {
                Firsts[parent] = group;
            }
            if (Rights[right] != 0) // If there are tokens to the right
            {
                Lefts[Rights[right]] = group;
            }
            else if (parent != 0) // If the right token is the last one and it has a parent
            {
                Lasts[parent] = group;
            }

            // Child tokens
            Lefts[left] = 0;
            Rights[right] = 0;
            token = left;
            while (token != 0)
            {
                Ups[token] = group;
                token = Rights[token];
                Counts[group]++;
            }
            Counts[parent] -= Counts[group] - 1;

            return token;
        }
        //[MethodImpl(MethodImplOptions.AggressiveInlining)]
        //public int Group(Enum type, int tokenStart, bool includeStart, int tokenEnd, bool includeEnd)
        //{
        //    return Group((long)(object)type, symbols[tokenStart] + symbols[tokenEnd], tokenStart, includeStart, tokenEnd, includeEnd);
        //}
        //[MethodImpl(MethodImplOptions.AggressiveInlining)]
        //public int Group(Enum type, string symbol, int tokenStart, bool includeStart, int tokenEnd, bool includeEnd)
        //{
        //    return Group((long)(object)type, symbol, tokenStart, includeStart, tokenEnd, includeEnd);
        //}
        public enum GroupMode
        {
            INCLUDE,
            EXCLUDE,
            DELETE,
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int Group(ulong type, int tokenStart, GroupMode modeStart, int tokenEnd, GroupMode modeEnd)
        {
            return Group(type, Symbols[tokenStart] + Symbols[tokenEnd], tokenStart, modeStart, tokenEnd, modeEnd);
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int Group(ulong type, string symbol, int tokenStart, GroupMode modeStart, int tokenEnd, GroupMode modeEnd)
        {
            int group = Create(type, symbol, TextStarts[tokenStart], TextEnds[tokenEnd]);

            int tokenStartActual = modeStart == GroupMode.INCLUDE ? tokenStart : Rights[tokenStart];
            int tokenEndActual = modeEnd == GroupMode.INCLUDE ? tokenEnd : Lefts[tokenEnd];

            // Delete needless tokens
            if (tokenStart == tokenEnd)
            {
                if (modeStart != GroupMode.INCLUDE && modeEnd == GroupMode.INCLUDE)
                {
                    throw new Exception("Cannot not include start token and include end token when it is the same token.");
                }
                if (modeStart == GroupMode.INCLUDE && modeEnd != GroupMode.INCLUDE)
                {
                    throw new Exception("Cannot include start token and not include end token when it is the same token.");
                }
                if (modeStart == GroupMode.DELETE && modeEnd == GroupMode.DELETE)
                {
                    Delete(tokenEnd);
                }
            }
            else
            {
                if (modeStart == GroupMode.DELETE)
                {
                    Delete(tokenStart);
                }
                if (modeEnd == GroupMode.DELETE)
                {
                    Delete(tokenEnd);
                }
            }

            // Group one token
            //if (tokenStartActual == tokenEndActual)
            //{
                //// Group
                //lefts[group] = lefts[tokenStartActual];
                //rights[group] = rights[tokenEndActual];
                //ups[group] = ups[tokenStartActual];
                //firsts[group] = tokenStartActual;
                //lasts[group] = tokenEndActual;
                //counts[group] = 1;
                //if (lefts[tokenStartActual] == 0 && ups[tokenStartActual] != 0)
                //{
                //    firsts[ups[tokenStartActual]] = group;
                //}
                //if (rights[tokenEndActual] == 0 && ups[tokenEndActual] != 0)
                //{
                //    lasts[ups[tokenEndActual]] = group;
                //}
                //// Child token
                //lefts[tokenStartActual] = 0;
                //rights[tokenStartActual] = 0;
                //ups[tokenStartActual] = group;
            //}
            //else
            //{
                // Surrounding tokens
                if (Lefts[tokenStartActual] != 0) // If there are more tokens to the left
                {
                    Rights[Lefts[tokenStartActual]] = group;
                    Lefts[group] = Lefts[tokenStartActual];
                }
                else if (Ups[tokenStartActual] != 0) // If the left token is the first one and it has a parent
                {
                    Firsts[Ups[tokenStartActual]] = group;
                }
                if (Rights[tokenEndActual] != 0) // If there are more tokens to the right
                {
                    Lefts[Rights[tokenEndActual]] = group;
                    Rights[group] = Rights[tokenEndActual];
                }
                else if (Ups[tokenEndActual] != 0) // If the right token is the last one and it has a parent
                {
                    Lasts[Ups[tokenEndActual]] = group;
                }
                Ups[group] = Ups[tokenStartActual];
                Firsts[group] = tokenStartActual;
                Lasts[group] = tokenEndActual;
                Counts[group] = 0;

                // Group tokens
                Lefts[tokenStartActual] = 0;
                Rights[tokenEndActual] = 0;
                int token = tokenStartActual;
                while (token != 0)
                {
                    Ups[token] = group;
                    token = Rights[token];
                    Counts[group]++;
                }
                Counts[Ups[group]] -= (Counts[group] - 1);
            //}

            return group;
        }
        //[MethodImpl(MethodImplOptions.AggressiveInlining)]
        //public int Merge(params int[] tokens)
        //{
        //    stringBuilder.Clear();
        //    int tokenCurrent = tokenStart;
        //    while (true)
        //    {
        //        stringBuilder.Append(symbols[tokenCurrent]);

        //        if (tokenCurrent == tokenEnd)
        //        {
        //            break;
        //        }
        //        tokenCurrent = rights[tokenCurrent];
        //        if (tokenCurrent == 0)
        //        {
        //            throw new Exception("Closing token not found in the line.");
        //        }
        //    }

        //    return AddNew(type, stringBuilder.ToString(), textStarts[tokenStart], textEnds[tokenEnd]);
        //}
        /// <summary>
        /// Merge all the children of the specified tokens into the first one.
        /// </summary>
        /// <param name="tokens">The tokens to merge.</param>
        /// <returns>The first token in which all tokens were merged.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int Merge(params int[] tokens)
        {
            int token = tokens[0];
            for (int i = 1; i < tokens.Length; i++)
            {
                MoveChildren(token, tokens[i]);
                Delete(tokens[i]);
            }
            return token;
        }
        /// <summary>
        /// Merge a token sequence into a new token.
        /// </summary>
        /// <param name="type">Type of the new token.</param>
        /// <param name="symbol">Symbol of the new token.</param>
        /// <param name="tokenStart">The first token to merge.</param>
        /// <param name="tokenEnd">The last token to merge.</param>
        /// <returns>The new token.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int Merge(ulong type, string symbol, int tokenStart, int tokenEnd)
        {
            if (tokenStart == tokenEnd)
            {
                return tokenStart;
            }

            // TODO: Reuse tokenStart instead of creating a new token

            int merging = Create(type, null /* assigned after the loop */ , TextStarts[tokenStart], TextEnds[tokenEnd]);
            int root = Ups[tokenStart];

            // Surrounding tokens
            if (Lefts[tokenStart] != 0) // If there are tokens to the left
            {
                Rights[Lefts[tokenStart]] = merging;
                Lefts[merging] = Lefts[tokenStart];
            }
            else if (Ups[tokenStart] != 0) // If the left token is the first one and it has a parent
            {
                Firsts[Ups[tokenStart]] = merging;
            }
            if (Rights[tokenEnd] != 0) // If there are tokens to the right
            {
                Lefts[Rights[tokenEnd]] = merging;
                Rights[merging] = Rights[tokenEnd];
            }
            else if (Ups[tokenEnd] != 0) // If the right token is the last one and it has a parent
            {
                Lasts[Ups[tokenEnd]] = merging;
            }
            Ups[merging] = Ups[tokenStart];

            int tokenCurrent = tokenStart;
            int tokenChildCurrentLast = 0;
            int tokensMergedCount = 0;
            while (true)
            {
                // If got children
                if (Firsts[tokenCurrent] != 0)
                {
                    // If there are already children transfered to the merging token -- connect the chain
                    if (tokenChildCurrentLast != 0)
                    {
                        Rights[tokenChildCurrentLast] = Firsts[tokenCurrent];
                        Lefts[Firsts[tokenCurrent]] = tokenChildCurrentLast;
                        Lasts[merging] = tokenChildCurrentLast;
                    }
                    // If there were no children yet -- point to the very first (this can still be zero)
                    if (Firsts[merging] == 0)
                    {
                        Firsts[merging] = Firsts[tokenCurrent];
                    }
                    // Transfer the children
                    int tokenChildNext = Firsts[tokenCurrent];
                    while (tokenChildNext != 0)
                    {
                        tokenChildCurrentLast = tokenChildNext;
                        Ups[tokenChildNext] = merging;
                        tokenChildNext = Rights[tokenChildNext];
                    }
                    Counts[merging] += Counts[tokenCurrent];
                }
                tokensMergedCount++;

                if (tokenCurrent == tokenEnd)
                {
                    break;
                }
                tokenCurrent = Rights[tokenCurrent];
                if (tokenCurrent == 0)
                {
                    throw new Exception("Closing token not found in the line.");
                }
            }
            Symbols[merging] = symbol;
            if (root != 0)
            {
                Counts[root] -= tokensMergedCount - 1;
            }

            return merging;
        }
        /// <summary>
        /// Merge a token sequence into a new token.
        /// </summary>
        /// <param name="type">Type of the new token.</param>
        /// <param name="tokenStart">The first token to merge.</param>
        /// <param name="tokenEnd">The last token to merge.</param>
        /// <returns>The new token.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int Merge(ulong type, int tokenStart, int tokenEnd)
        {
            if (tokenStart == tokenEnd)
            {
                return tokenStart;
            }

            // TODO: Reuse tokenStart instead of creating a new token

            StringBuilder.Clear();
            int merging = Create(type, null /* assigned after the loop */ , TextStarts[tokenStart], TextEnds[tokenEnd]);
            int root = Ups[tokenStart];

            // Surrounding tokens
            if (Lefts[tokenStart] != 0) // If there are tokens to the left
            {
                Rights[Lefts[tokenStart]] = merging;
                Lefts[merging] = Lefts[tokenStart];
            }
            else if (Ups[tokenStart] != 0) // If the left token is the first one and it has a parent
            {
                Firsts[Ups[tokenStart]] = merging;
            }
            if (Rights[tokenEnd] != 0) // If there are tokens to the right
            {
                Lefts[Rights[tokenEnd]] = merging;
                Rights[merging] = Rights[tokenEnd];
            }
            else if (Ups[tokenEnd] != 0) // If the right token is the last one and it has a parent
            {
                Lasts[Ups[tokenEnd]] = merging;
            }
            Ups[merging] = Ups[tokenStart];

            int tokenCurrent = tokenStart;
            int tokenChildCurrentLast = 0;
            int tokensMergedCount = 0;
            while (true)
            {
                // If got children
                if (Firsts[tokenCurrent] != 0)
                {
                    // If there are already children transfered to the merging token -- connect the chain
                    if (tokenChildCurrentLast != 0)
                    {
                        Rights[tokenChildCurrentLast] = Firsts[tokenCurrent];
                        Lefts[Firsts[tokenCurrent]] = tokenChildCurrentLast;
                        Lasts[merging] = tokenChildCurrentLast;
                    }
                    // If there were no children yet -- point to the very first (this can still be zero)
                    if (Firsts[merging] == 0)
                    {
                        Firsts[merging] = Firsts[tokenCurrent];
                    }
                    // Transfer the children
                    int tokenChildNext = Firsts[tokenCurrent];
                    while (tokenChildNext != 0)
                    {
                        tokenChildCurrentLast = tokenChildNext;
                        Ups[tokenChildNext] = merging;
                        tokenChildNext = Rights[tokenChildNext];
                    }
                    Counts[merging] += Counts[tokenCurrent];
                }
                tokensMergedCount++;
                StringBuilder.Append(Symbols[tokenCurrent]);

                if (tokenCurrent == tokenEnd)
                {
                    break;
                }
                tokenCurrent = Rights[tokenCurrent];
                if (tokenCurrent == 0)
                {
                    throw new Exception("Closing token not found in the line.");
                }
            }
            Symbols[merging] = StringBuilder.ToString();
            if (root != 0)
            {
                Counts[root] -= tokensMergedCount - 1;
            }

            return merging;
        }
        //[MethodImpl(MethodImplOptions.AggressiveInlining)]
        //public void MoveTokenRightOf(int token, int destination)
        //{
        //    // Old place left and right
        //    {
        //        int left = lefts[token];
        //        int right = rights[token];
        //        if (left != 0)
        //        {
        //            if (right != 0)
        //            {
        //                rights[left] = lefts[right];
        //            }
        //            else // right == 0
        //            {
        //                rights[left] = 0;
        //            }
        //        }
        //        else
        //        {
        //            if (right != 0)
        //            {
        //                lefts[right] = 0;
        //            }
        //            else // right == 0
        //            {
        //                ;
        //            }
        //        }
        //    }

        //    // Old place down
        //    {
        //        int down = downs[token];
        //        if (down != 0)
        //        {
        //            // Actually, nothing needs to be removed as nothing now links down there.
        //            // Yes, it occupies memory, but moving stuff would be much of a hassle.
        //        }
        //    }

        //    // Old place up
        //    {
        //        int up = ups[token];
        //        if (up != 0 /* there is a parent */ && downs[up] == token /* the parent points down to this exact token */)
        //        {
        //            downs[up] = rights[token]; // Point the parent to the next token in the row
        //        }
        //    }
        //    // Token itself
        //    {
        //        lefts[token] = destination;
        //        rights[token] = rights[destination];
        //        ups[token] = ups[destination];
        //    }
        //    // New place
        //    {
        //        if (rights[destination] != 0)
        //        {
        //            lefts[rights[destination]] = token;
        //        }
        //        rights[destination] = token;
        //    }
        //}
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void PasteLeft(int right, params int[] tokens)
        {
            for (int i = tokens.Length - 1; i >= 0; i--)
            {
                PasteLeft(right, tokens[i]);
            }
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void PasteLeft(int right, int token)
        {
            int left = Lefts[right];

            Lefts[token] = left;
            Rights[token] = right;
            Ups[token] = Ups[right];
            Counts[Ups[right]]++;

            Lefts[right] = token;
            if (left != 0)
            {
                Rights[left] = token;
            }
            else
            {
                Firsts[Ups[right]] = token;
            }
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void PasteRight(int left, params int[] tokens)
        {
            for (int i = 0; i < tokens.Length; i++)
            {
                PasteRight(left, tokens[i]);
            }
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void PasteRight(int left, int token)
        {
            int right = Rights[left];

            Lefts[token] = left;
            Rights[token] = right;
            Ups[token] = Ups[left];
            Counts[Ups[left]]++;

            Rights[left] = token;
            if (right != 0)
            {
                Lefts[right] = token;
            }
            else
            {
                Lasts[Ups[left]] = token;
            }
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void PasteUnder(int root, params int[] tokens)
        {
            if (tokens.Length == 0)
            {
                return;
            }
            PasteUnder(root, tokens[0]);
            for (int i = 1; i < tokens.Length; i++)
            {
                PasteLastUnder(root, tokens[i]);
            }
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void PasteUnder(int root, int token)
        {
            //// If there are already some children
            //if (downs[root] != 0)
            //{
            //    return false;
            //}

            Rights[token] = 0;
            Lefts[token] = 0;

            Firsts[root] = token;
            Lasts[root] = token;

            Ups[token] = root;
            Counts[root] = 1;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void PasteLastUnder(int root, params int[] tokens)
        {
            throw new NotImplementedException();
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void PasteLastUnder(int root, int token)
        {
            // If there are no children yet
            if (Firsts[root] == 0)
            {
                // Set the token
                Lefts[token] = 0;
                Rights[token] = 0;
                Ups[token] = root;
                // Set the root
                Firsts[root] = token;
                Lasts[root] = token;
            }
            // Some children already exist, place the new one at the end
            else
            {
                // Interconnect the token with the last child token
                Rights[Lasts[root]] = token;
                Lefts[token] = Lasts[root];
                // Set the token
                Rights[token] = 0;
                Ups[token] = root;
                // Set the root
                Lasts[root] = token;
            }
            Counts[root]++;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void PasteFirstUnder(int root, params int[] tokens)
        {
            throw new NotImplementedException();
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void PasteFirstUnder(int root, int token)
        {
            // If there are no children yet
            if (Firsts[root] == 0)
            {
                // Set the token
                Lefts[token] = 0;
                Rights[token] = 0;
                Ups[token] = root;
                // Set the root
                Firsts[root] = token;
                Lasts[root] = token;
            }
            // Some children already exist, place the new one at the start
            else
            {
                // Interconnect the token with the first child token
                Lefts[Firsts[root]] = token;
                Rights[token] = Firsts[root];
                // Set the token
                Lefts[token] = 0;
                Ups[token] = root;
                // Set the root
                Firsts[root] = token;
            }
            Counts[root]++;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int CutBlock(int left, int right)
        {
            // If just one token
            if (left == right)
            {
                Cut(left);
                return 1;
            }

            // If there are tokens to the left
            if (Lefts[left] != 0)
            {
                Rights[Lefts[left]] = Rights[right];
            }
            // If left token is the first token
            else
            {
                Firsts[Ups[left]] = Rights[right];
            }

            // If there are tokens to the right
            if (Rights[right] != 0)
            {
                Lefts[Rights[right]] = Lefts[left];
            }
            // If right token is the last token
            else
            {
                Lasts[Ups[left]] = Lefts[left];
            }

            int count = 1;
            for (int token = left; token != right && token != 0; token = Rights[token])
            {
                count++;
            }
            Ups[left] -= count;
            return count;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void MoveBlockLeftOf(int destination, int left, int right, int count)
        {
            // Up
            int up = Ups[destination];
            Counts[up] += count;
            int token = left;
            Ups[left] = destination;
            while (token != right)
            {
                token = Rights[token];
                Ups[token] = up;
            }

            // If gonna be first
            if (Lefts[destination] == 0)
            {
                // Down from parent
                Firsts[Ups[destination]] = left;
                // Right
                Lefts[destination] = right;
                Rights[right] = destination;
            }
            else
            {
                // Left
                Rights[Lefts[destination]] = left;
                Lefts[left] = Lefts[destination];
                // Right
                Lefts[destination] = right;
                Rights[right] = destination;
            }
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void MoveBlockRightOf(int destination, int left, int right, int count)
        {
            // Up
            int up = Ups[destination];
            Counts[up] += count;
            int token = left;
            Ups[left] = destination;
            while (token != right)
            {
                token = Rights[token];
                Ups[token] = up;
            }

            // If gonna be last
            if (Rights[destination] == 0)
            {
                // Left
                Rights[destination] = left;
                Lefts[left] = destination;
            }
            else
            {
                // Left
                Rights[destination] = left;
                Lefts[left] = destination;
                // Right
                Lefts[Rights[destination]] = right;
                Rights[right] = Rights[destination];
            }
        }
        #endregion

        #region Util
        //[MethodImpl(MethodImplOptions.AggressiveInlining)]
        //public int GetEnum(Type type)
        //{
        //    if (enumToInt.TryGetValue(type, out int @int))
        //    {
        //        return @int;
        //    }
        //    @int = enumToInt.Count;
        //    enumToInt.Add(type, @int);
        //    return @int;
        //}
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void ExpandTrash()
        {
            // Allocate memory
            int[] trashExpanded = new int[Trash.Length * 2 /* expand twice */];
            // Copy data
            Trash.CopyTo(trashExpanded, 0);
            // Assign
            Trash = trashExpanded;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void Expand()
        {
            // Expand twice
            Capacity *= 2;
            // Allocate memory
            ulong[] newTypes     = new ulong[Capacity];
            string[] newSymbols = new string[Capacity];
            int[] newTextStarts = new int[Capacity];
            int[] newTextEnds   = new int[Capacity];
            object[] newObjects = new object[Capacity];
            int[] newLefts      = new int[Capacity];
            int[] newRights     = new int[Capacity];
            int[] newUps        = new int[Capacity];
            int[] newFirsts      = new int[Capacity];
            int[] newLasts      = new int[Capacity];
            int[] newCounts     = new int[Capacity];
            int[] newPasses     = new int[Capacity];
            // Copy data
            Types.CopyTo(newTypes, 0);
            Symbols.CopyTo(newSymbols, 0);
            TextStarts.CopyTo(newTextStarts, 0);
            TextEnds.CopyTo(newTextEnds, 0);
            Objects.CopyTo(newObjects, 0);
            Lefts.CopyTo(newLefts, 0);
            Rights.CopyTo(newRights, 0);
            Ups.CopyTo(newUps, 0);
            Firsts.CopyTo(newFirsts, 0);
            Lasts.CopyTo(newLasts, 0);
            Counts.CopyTo(newCounts, 0);
            Passes.CopyTo(newPasses, 0);
            // Replace
            Types = newTypes;
            Symbols = newSymbols;
            TextStarts = newTextStarts;
            TextEnds = newTextEnds;
            Objects = newObjects;
            Lefts = newLefts;
            Rights = newRights;
            Ups = newUps;
            Firsts = newFirsts;
            Lasts = newLasts;
            Counts = newCounts;
            Passes = newPasses;
        }
        public int CheckConsistency()
        {
            for (int rootIndex = 0; rootIndex < Roots.Count; rootIndex++)
            {
                int root = Roots[rootIndex];
                int token = root;
                while (token != 0)
                {
                    // Check vertical connections
                    if (Firsts[token] != 0 && Ups[Firsts[token]] != token)
                    {
                        return token;
                        //throw new Exception($"Token {token} first child {firsts[token]} ");
                    }
                    if (Lasts[token] != 0 && Ups[Lasts[token]] != token)
                    {
                        return token;
                        //throw new Exception();
                    }
                    // Check horizontal connections
                    if (Rights[token] != 0 && Lefts[Rights[token]] != token)
                    {
                        return token;
                        //throw new Exception();
                    }
                    if (Lefts[token] != 0 && Rights[Lefts[token]] != token)
                    {
                        return token;
                        //throw new Exception();
                    }
                    //// Check references to the original text
                    //if (lefts[token] != 0 && textEnds[lefts[token]] >= textStarts[token])
                    //{
                    //    throw new Exception();
                    //}
                    //if (rights[token] != 0 && textStarts[rights[token]] <= textEnds[token])
                    //{
                    //    throw new Exception();
                    //}

                    token = GetNext(token);
                }
            }
            return 0;
        }
        //public string PrintConsistency(Parser parser)
        //{

        //}
        //public string PrintSimple(Parser parser, int padding = 32)
        //{
        //}
        public string PrintSimple(Parser parser, int padding = 64)
        {
            Stack<int> stack = new Stack<int>(1024);
            List<bool> down = new List<bool>(1024);
            int token = 1;
            stack.Push(0);
            down.Add(true);
            //down[0] = false;

            StringBuilder stringBuilder = new StringBuilder();
            //stringBuilder.Append("┌");

            // Print all tokens as a padded tree
            while (stack.Count > 0)
            {
                down[down.Count - 1] = Rights[token] != 0;
                string symbol = Symbols[token] == null ? "" : Symbols[token].Replace("\r", "\\r").Replace("\n", "\\n");

                // Root
                if (stack.Count == 1)
                {
                    stringBuilder.Append($"┌ {symbol}");
                    //stringBuilder.Append($"○ {symbol}");
                    //stringBuilder.Append($"# {symbol}");
                }
                else
                {
                    // Print tree
                    for (int i = 1; i < down.Count - 1; i++)
                    {
                        stringBuilder.Append(down[i] ? "│" : " ");
                    }
                    stringBuilder.Append(Rights[token] != 0 ? "├" : "└");
                    stringBuilder.Append(Firsts[token] == 0 ? "─" : symbol != "" ? "┬" : "┐");
                    stringBuilder.Append(" ");
                    stringBuilder.Append(symbol);
                }

                // Print padding
                int lineLength = stack.Count * 1 + symbol.Length;
                while (lineLength++ < padding)
                {
                    stringBuilder.Append(" ");
                }

                // Print type
                Enum @enum = parser.ConvertTokenIdToEnum(Types[token]);
                string type = $" {@enum} ({@enum.GetType().Name})";// ({counts[token]})";
                stringBuilder.AppendLine(type);


                // Go down
                if (Firsts[token] != 0)
                {
                    stack.Push(token);
                    token = Firsts[token];
                    down.Add(Rights[token] != 0);
                    continue;
                }

                // Go right
                if (Rights[token] != 0)
                {
                    token = Rights[token];
                    down[down.Count - 1] = Rights[token] != 0;
                    continue;
                }

                // Go back up and right
                while (token != 0 && Rights[token] == 0)
                {
                    token = stack.Pop();
                    down.RemoveAt(down.Count - 1);
                }
                token = Rights[token];
            }

            return stringBuilder.ToString();
        }
        //public string PrintSimple(Parser parser, int padding = 32)
        //{
        //    Stack<int> stack = new Stack<int>(1024);
        //    int token = 1;
        //    stack.Push(0);

        //    StringBuilder stringBuilder = new StringBuilder();

        //    // Print all tokens as a padded tree
        //    while (stack.Count > 0)
        //    {
        //        // Print type
        //        Enum @enum = parser.ConvertTokenIdToEnum(types[token]);
        //        string type = $"{@enum} ({@enum.GetType().Name})";// ({counts[token]})";
        //        // Print padding
        //        for (int i = 1; i < stack.Count; i++)
        //        {
        //            stringBuilder.Append(" ");
        //        }
        //        stringBuilder.Append(type);
        //        stringBuilder.Append(" ");
        //        for (int i = type.Length + 1; i < padding; i++)
        //        {
        //            stringBuilder.Append(" ");
        //        }
        //        // Print padding
        //        for (int i = 1; i < stack.Count; i++)
        //        {
        //            stringBuilder.Append("  ");
        //        }
        //        // Print symbol
        //        stringBuilder.AppendLine(symbols[token] == null ? "" : symbols[token].Replace("\r", "\\r").Replace("\n", "\\n"));

        //        // Go down
        //        if (firsts[token] != 0)
        //        {
        //            stack.Push(token);
        //            token = firsts[token];
        //            continue;
        //        }

        //        // Go right
        //        if (rights[token] != 0)
        //        {
        //            token = rights[token];
        //            continue;
        //        }

        //        // Go back up and right
        //        while (token != 0 && rights[token] == 0)
        //        {
        //            token = stack.Pop();
        //        }
        //        token = rights[token];
        //    }

        //    return stringBuilder.ToString();
        //}
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static int GetNearestDown<T>(SortedList<int, T> sortedList, int key)
        {
            if (sortedList == null)
            {
                throw new ArgumentNullException(nameof(sortedList));
            }
            if (sortedList.Count == 0)
            {
                throw new ArgumentException("SortedList cannot be empty.", nameof(sortedList));
            }
            // Check outer borders and precise values
            {
                int keyFirst = sortedList.Keys[0];
                if (key <= keyFirst)
                {
                    return keyFirst;
                }
                int keyLast = sortedList.Keys[sortedList.Keys.Count - 1];
                if (keyLast <= key)
                {
                    return keyLast;
                }
                if (sortedList.ContainsKey(key))
                {
                    return key;
                }
            }

            int keyIndexMinimum = 0;
            int keyIndexMaximum = sortedList.Count - 1;
            int keyIndexCurrent = 0;
            while ((keyIndexCurrent = (keyIndexMinimum + keyIndexMaximum) / 2) != keyIndexMinimum)
            {
                //keyIndexCurrent = (keyIndexMinimum + keyIndexMaximum) / 2;
                if (sortedList.Keys[keyIndexCurrent] < key)
                {
                    keyIndexMinimum = keyIndexCurrent;
                }
                else if (sortedList.Keys[keyIndexCurrent] > key)
                {
                    keyIndexMaximum = keyIndexCurrent;
                }
                else
                {
                    break;
                }
            }
            return sortedList.Keys[keyIndexCurrent];
        }
        #endregion
    }
}
