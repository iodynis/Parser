﻿namespace Iodynis.Libraries.Parsing
{
    public struct Token
    {
        public readonly int Type;
        public readonly string Symbol;
        public readonly int TextStartIndex;
        public readonly int TextStopIndex;

        public Token(int type, string symbol, int textStartIndex, int textStopIndex)
        {
            Type = type;
            Symbol = symbol;
            TextStartIndex = textStartIndex;
            TextStopIndex = textStopIndex;
        }
        //public Token(Enum type, string symbol, int textStartIndex, int textStopIndex)
        //{
        //    Type = type;
        //    Symbol = symbol;
        //    TextStartIndex = textStartIndex;
        //    TextStopIndex = textStopIndex;
        //}

        public string Print()
        {
            return $"Symbol {Symbol} at [{TextStartIndex}, {TextStopIndex}]";
        }

        public override string ToString()
        {
            return Symbol;
        }
    }
}
