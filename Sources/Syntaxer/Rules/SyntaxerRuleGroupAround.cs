﻿using System;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleGroupAround : SyntaxerRule
    {
        private Enum EnumGroup;
        private ulong TypeGroup = 0;
        private string SymbolGroup;
        private Enum EnumToken;
        private ulong TypeToken;

        private int LeftCount;
        private int RightCount;

        public SyntaxerRuleGroupAround(Enum token, int leftCount, int rightCount)
            : base(token)
        {
            EnumGroup   = null;
            SymbolGroup = null;

            EnumToken  = token;
            LeftCount  = leftCount;
            RightCount = rightCount;
        }
        public SyntaxerRuleGroupAround(Enum token, Enum tokenMatched, int tokensLeftCount, int tokensRightCount)
            : this(tokenMatched, tokensLeftCount, tokensRightCount)
        {
            EnumGroup = token;
            SymbolGroup = null;
        }
        public SyntaxerRuleGroupAround(Enum token, string symbol, Enum tokenMatched, int tokensLeftCount, int tokensRightCount)
            : this(tokenMatched, tokensLeftCount, tokensRightCount)
        {
            EnumGroup = token;
            SymbolGroup = symbol;
        }
        protected override void CustomInit()
        {
            TypeGroup = Parser.ConvertEnumToTokenId(EnumGroup);
            TypeToken = MatchedTypes[0];
        }
        protected override void CustomDeinit()
        {
            TypeGroup = 0;
            TypeToken = 0;
        }
        public override bool Process(ref int token)
        {
            int left = TokenManager.GetLeft(token, LeftCount);
            if (left == 0)
            {
                return false;
            }
            int right = TokenManager.GetRight(token, RightCount);
            if (right == 0)
            {
                return false;
            }

            token = TokenManager.GroupAround(TypeGroup, SymbolGroup, token, LeftCount, RightCount); // Redo

            return true;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer Group(this Syntaxer syntaxer, Enum token, Enum tokenMatched, int tokensLeftCount, int tokensRightCount)
        {
            syntaxer.AddRule(new SyntaxerRuleGroupAround(token, tokenMatched, tokensLeftCount, tokensRightCount));
            return syntaxer;
        }
        //public static Syntaxer OperatorUnary(this Syntaxer syntaxer, Enum type)
        //{
        //    syntaxer.AddRule(new SyntaxerRuleGroupAround(type, 0, 1));
        //    return syntaxer;
        //}
        //public static Syntaxer OperatorBinary(this Syntaxer syntaxer, Enum type)
        //{
        //    syntaxer.AddRule(new SyntaxerRuleGroupAround(type, 1, 1));
        //    return syntaxer;
        //}
        //public static Syntaxer OperatorTernary(this Syntaxer syntaxer, Enum type)
        //{
        //    syntaxer.AddRule(new SyntaxerRuleGroupAround(type, 1, 2));
        //    return syntaxer;
        //}
    }
}
