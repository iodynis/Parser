﻿using System;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleReplace : SyntaxerRule
    {
        private Enum EnumNew;
        private ulong TypeNew = 0;
        private string SymbolNew;
        private Enum EnumOld;
        private ulong TypeOld = 0;
        private string SymbolOld;

        public SyntaxerRuleReplace(Enum token, Enum tokenMatched)
            : this(token, null, tokenMatched, null) { }
        public SyntaxerRuleReplace(Enum token, string symbol, Enum tokenMatched, string symbolMatched)
            : base(tokenMatched)
        {
            EnumNew = token;
            SymbolNew = symbol;
            EnumOld = tokenMatched;
            SymbolOld = symbolMatched;
        }
        protected override void CustomInit()
        {
            TypeNew = Parser.ConvertEnumToTokenId(EnumNew);
            TypeOld = MatchedTypes[0];
        }
        protected override void CustomDeinit()
        {
            TypeNew = 0;
            TypeOld = 0;
        }
        public override bool Process(ref int token)
        {
            if (TypeOld >= 0 && TypeOld != TokenManager.GetType(token))
            {
                return false;
            }
            if (SymbolOld != null && SymbolOld != TokenManager.GetSymbol(token))
            {
                return false;
            }
            if (TypeNew >= 0)
            {
                TokenManager.SetType(token, TypeNew);
            }
            if (SymbolNew != null)
            {
                TokenManager.SetSymbol(token, SymbolNew);
            }
            return true;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer Replace(this Syntaxer syntaxer, Enum token, Enum tokenMatched)
        {
            syntaxer.AddRule(new SyntaxerRuleReplace(token, tokenMatched));
            return syntaxer;
        }
        public static Syntaxer Replace(this Syntaxer syntaxer, string symbol, string symbolMatched)
        {
            syntaxer.AddRule(new SyntaxerRuleReplace(null, symbol, null, symbolMatched));
            return syntaxer;
        }
        public static Syntaxer Replace(this Syntaxer syntaxer, Enum token, string symbol, Enum tokenMatched, string symbolMatched)
        {
            syntaxer.AddRule(new SyntaxerRuleReplace(token, symbol, tokenMatched, symbolMatched));
            return syntaxer;
        }
    }
}
