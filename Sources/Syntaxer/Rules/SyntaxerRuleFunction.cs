﻿using System;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleFunction : SyntaxerRule
    {
        private Enum EnumFunction;
        private ulong TypeFunction = 0;
        private Enum EnumArgument;
        private ulong TypeArgument = 0;
        //private string symbolArgument;
        private Enum EnumMatchFunctionName;
        private ulong TypeMatchFunctionName = 0;
        private Enum EnumMatchFunctionArguments;
        private ulong TypeMatchFunctionArguments = 0;
        private Enum EnumMatchFunctionArgumentsDelimiter;
        private ulong TypeMatchFunctionArgumentsDelimiter = 0;

        public SyntaxerRuleFunction(Enum tokenFunction, Enum tokenArgument, Enum functionName, Enum functionArguments, Enum functionArgumentsDelimiter)
        //    : this(token, functionName, functionarguments, functionArgumentsDelimiter) { }
        //public SyntaxerRuleFunction(Enum function, Enum argument, string argumentSymbol, Enum name, Enum arguments, Enum delimiter)
            : base(functionName)
        {
            EnumFunction = tokenFunction;
            EnumArgument = tokenArgument;
            //this.symbolArgument = argumentSymbol;
            EnumMatchFunctionName       = functionName;
            EnumMatchFunctionArguments  = functionArguments;
            EnumMatchFunctionArgumentsDelimiter = functionArgumentsDelimiter;
        }
        protected override void CustomInit()
        {
            TypeFunction                        = Parser.ConvertEnumToTokenId(EnumFunction);
            TypeArgument                        = Parser.ConvertEnumToTokenId(EnumArgument);
            TypeMatchFunctionName               = MatchedTypes[0];
            TypeMatchFunctionArguments          = Parser.ConvertEnumToTokenId(EnumMatchFunctionArguments);
            TypeMatchFunctionArgumentsDelimiter = Parser.ConvertEnumToTokenId(EnumMatchFunctionArgumentsDelimiter);
        }
        protected override void CustomDeinit()
        {
            TypeFunction                        = 0;
            TypeArgument                        = 0;
            TypeMatchFunctionName               = 0;
            TypeMatchFunctionArguments          = 0;
            TypeMatchFunctionArgumentsDelimiter = 0;
        }
        public override bool Process(ref int token)
        {
            int tokenArguments = TokenManager.GetRight(token);
            if (TypeMatchFunctionArguments != TokenManager.GetType(tokenArguments))
            {
                return false;
            }

            // Leave only the token with arguments underneath it
            //string symbolFunction = TokenManager.GetSymbol(token);
            //TokenManager.Delete(token);

            //TokenManager.DeleteChildrenOfType(tokenArguments, typeFunctionArgumentsDelimiter);
            //TokenManager.MoveChildren(token, tokenArguments);
            //TokenManager.Delete(tokenArguments);
            //TokenManager.SetType(token, typeToken);

            // Split the arguments
            TokenManager.SplitChildren(tokenArguments, TypeArgument, TypeMatchFunctionArgumentsDelimiter);

            // Merge arguments into the function
            token = TokenManager.Merge(token, tokenArguments);
            //token = TokenManager.Merge(typeFunction, TokenManager.GetSymbol(token), token, tokenArguments);

            // Set the name and the type of the function
            TokenManager.SetType(token, TypeFunction);

            //token = tokenArguments;
            return true;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer Function(this Syntaxer syntaxer, Enum tokenFunction, Enum tokenArgument, Enum functionName, Enum functionArguments, Enum functionArgumentsDelimiter)
        {
            syntaxer.AddRule(new SyntaxerRuleFunction(tokenFunction, tokenArgument, functionName, functionArguments, functionArgumentsDelimiter));
            return syntaxer;
        }
        //public static Syntaxer Function(this Syntaxer syntaxer, Enum function, Enum argument, string argumentSymbol, Enum name, Enum arguments, Enum delimiter)
        //{
        //    syntaxer.AddRule(new SyntaxerRuleFunction(function, argument, argumentSymbol, name, arguments, delimiter));
        //    return syntaxer;
        //}
    }
}
