﻿using System;
using System.Collections.Generic;

namespace Iodynis.Libraries.Parsing
{
    public abstract class SyntaxerRule
	{
        private HashSet<Enum> MatchedEnums { get; }
        internal List<ulong> MatchedTypes { get; } = new List<ulong>();
        public Parser Parser { get; private set; }
        public Syntaxer Syntaxer { get; private set; }
        public SyntaxerPass SyntaxerPass { get; private set; }
        public TokenManager TokenManager { get; private set; }

		protected SyntaxerRule()
        {
            MatchedEnums = new HashSet<Enum>();
        }
        /// <summary>
        /// Syntaxer rule.
        /// </summary>
        /// <param name="token">Matched token.</param>
		protected SyntaxerRule(Enum token)
        {
            if (token == null)
            {
                throw new ArgumentNullException(nameof(token));
            }

            MatchedEnums = new HashSet<Enum>() { token };
        }
        /// <summary>
        /// Syntaxer rule.
        /// </summary>
        /// <param name="tokens">Matched tokens.</param>
		protected SyntaxerRule(IEnumerable<Enum> tokens)
        {
            if (tokens == null)
            {
                throw new ArgumentNullException(nameof(tokens));
            }

            MatchedEnums = new HashSet<Enum>(tokens);
        }
        internal void Init(SyntaxerPass syntaxerPass)
        {
            // Set references
            Parser = syntaxerPass.Parser;
            Syntaxer = syntaxerPass.Syntaxer;
            SyntaxerPass = syntaxerPass;
            TokenManager = syntaxerPass.TokenManager;
            // Enums to ids
            foreach (Enum @enum in MatchedEnums)
            {
                MatchedTypes.Add(Parser.ConvertEnumToTokenId(@enum));
            }

            // Custom
            CustomInit();
        }
        protected virtual void CustomInit()
        {
            ;
        }
        internal void Deinit()
        {
            // Custom
            CustomDeinit();

            // Clear ids
            MatchedTypes.Clear();
            // Clear references
            TokenManager = null;
            SyntaxerPass = null;
            Syntaxer = null;
            Parser = null;
        }
        protected virtual void CustomDeinit()
        {
            TokenManager = null;
        }
        public virtual bool Process(ref int token)
        {
            throw new NotImplementedException();
        }
        protected void Match(Enum @enum)
        {
            MatchedEnums.Add(@enum);
        }
        protected void Match(IEnumerable<Enum> enums)
        {
            foreach (Enum @enum in enums)
            {
                MatchedEnums.Add(@enum);
                MatchedTypes.Add(Parser.ConvertEnumToTokenId(@enum));
            }
        }
	}
}
