﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleOperatorBinaryWithFilter : SyntaxerRule
    {
        private readonly Enum EnumNew;
        private ulong TypeNew = 0;
        private readonly Enum EnumOld;
        private ulong TypeOld = 0;
        private readonly Enum[] EnumWhitelistLeft;
        private ulong[] TypeWhitelistLeft;
        private readonly Enum[] EnumBlacklistLeft;
        private ulong[] TypeBlacklistLeft;
        private readonly Enum[] EnumWhitelistRight;
        private ulong[] TypeWhitelistRight;
        private readonly Enum[] EnumBlacklistRight;
        private ulong[] TypeBlacklistRight;

        public SyntaxerRuleOperatorBinaryWithFilter(Enum token, IEnumerable<Enum> tokenWwhitelistLeft, IEnumerable<Enum> tokenWhitelistRight, IEnumerable<Enum> tokenBlacklistLeft, IEnumerable<Enum> tokenBlacklistRight)
            : this(token, token, tokenWwhitelistLeft, tokenWhitelistRight, tokenBlacklistLeft, tokenBlacklistRight) { }
        public SyntaxerRuleOperatorBinaryWithFilter(Enum token, Enum tokenMatched, IEnumerable<Enum> tokenWhitelistLeft, IEnumerable<Enum> tokenWhitelistRight, IEnumerable<Enum> tokenBlacklistLeft, IEnumerable<Enum> tokenBlacklistRight)
            : base(token)
        {
            EnumNew = token;
            EnumOld = tokenMatched;
            EnumWhitelistLeft  = tokenWhitelistLeft?.ToArray()  ?? new Enum[0];
            EnumBlacklistLeft  = tokenBlacklistLeft?.ToArray()  ?? new Enum[0];
            EnumWhitelistRight = tokenWhitelistRight?.ToArray() ?? new Enum[0];
            EnumBlacklistRight = tokenBlacklistRight?.ToArray() ?? new Enum[0];
        }
        protected override void CustomInit()
        {
            TypeNew = Parser.ConvertEnumToTokenId(EnumNew);
            TypeOld = MatchedTypes[0];
            TypeWhitelistLeft  = EnumWhitelistLeft .Select(_enum => Parser.ConvertEnumToTokenId(_enum)).ToArray();
            TypeBlacklistLeft  = EnumBlacklistLeft .Select(_enum => Parser.ConvertEnumToTokenId(_enum)).ToArray();
            TypeWhitelistRight = EnumWhitelistRight.Select(_enum => Parser.ConvertEnumToTokenId(_enum)).ToArray();
            TypeBlacklistRight = EnumBlacklistRight.Select(_enum => Parser.ConvertEnumToTokenId(_enum)).ToArray();
        }
        protected override void CustomDeinit()
        {
            TypeNew = 0;
            TypeOld = 0;
            TypeWhitelistLeft  = null;
            TypeBlacklistLeft  = null;
            TypeWhitelistRight = null;
            TypeBlacklistRight = null;
        }
        public override bool Process(ref int token)
        {
            int left = TokenManager.GetLeft(token);
            if (left == 0)
            {
                return false;
            }
            ulong leftType = TokenManager.GetType(left);
            if ((TypeWhitelistLeft.Length > 0 && !TypeWhitelistLeft.Contains(leftType)) || (TypeBlacklistLeft.Length > 0 && TypeBlacklistLeft.Contains(leftType)))
            {
                return false;
            }

            int right = TokenManager.GetRight(token);
            if (right == 0)
            {
                return false;
            }
            ulong rightType = TokenManager.GetType(right);
            if ((TypeWhitelistRight.Length > 0 && !TypeWhitelistRight.Contains(rightType)) || (TypeBlacklistRight.Length > 0 && TypeBlacklistRight.Contains(rightType)))
            {
                return false;
            }

            TokenManager.Cut(left);
            TokenManager.Cut(right);

            TokenManager.PasteFirstUnder(token, left);
            TokenManager.PasteLastUnder(token, right);

            if (TypeNew >= 0)
            {
                TokenManager.SetType(token, TypeNew);
            }

            return true;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer Binary(this Syntaxer syntaxer, Enum token, Enum tokenMatched, Enum tokenWhitelistLeft, Enum tokenWhitelistRight)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorBinaryWithFilter(token, tokenMatched, new Enum[] { tokenWhitelistLeft }, new Enum[] { tokenWhitelistRight }, null, null));
            return syntaxer;
        }
        public static Syntaxer Binary(this Syntaxer syntaxer, Enum token, Enum tokenMatched, IEnumerable<Enum> tokenWhitelistLeft, IEnumerable<Enum> tokenWhitelistRight)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorBinaryWithFilter(token, tokenMatched, tokenWhitelistLeft, tokenWhitelistRight, null, null));
            return syntaxer;
        }
        public static Syntaxer Binary(this Syntaxer syntaxer, Enum token, Enum tokenMatched, IEnumerable<Enum> tokenWhitelistLeft, IEnumerable<Enum> tokenWhitelistRight, IEnumerable<Enum> tokenBlacklistLeft, IEnumerable<Enum> tokenBlacklistRight)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorBinaryWithFilter(token, tokenMatched, tokenWhitelistLeft, tokenWhitelistRight, tokenBlacklistLeft, tokenBlacklistRight));
            return syntaxer;
        }
    }
}
