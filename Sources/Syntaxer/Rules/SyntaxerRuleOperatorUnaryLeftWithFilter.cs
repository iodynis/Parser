﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleOperatorUnaryLeftWithFilter : SyntaxerRule
    {
        private Enum EnumNew;
        private ulong TypeNew = 0;
        private Enum EnumOld;
        private ulong TypeOld = 0;
        private Enum[] EnumWhitelistRight;
        private ulong[] TypeWhitelistRight;
        private Enum[] EenumBlacklistRight;
        private ulong[] TypeBlacklistRight;

        public SyntaxerRuleOperatorUnaryLeftWithFilter(Enum token, IEnumerable<Enum> tokenWhitelistRight, IEnumerable<Enum> tokenBlacklistRight)
            : this(token, token, tokenWhitelistRight, tokenBlacklistRight) { }
        public SyntaxerRuleOperatorUnaryLeftWithFilter(Enum token, Enum tokenMatched, IEnumerable<Enum> tokenWhitelistRight, IEnumerable<Enum> tokenBlacklistRight)
        {
            EnumNew = token;
            EnumOld = tokenMatched;
            EnumWhitelistRight = tokenWhitelistRight?.ToArray() ?? new Enum[0];
            EenumBlacklistRight = tokenBlacklistRight?.ToArray() ?? new Enum[0];

            Match(EnumOld);
        }
        protected override void CustomInit()
        {
            TypeNew = Parser.ConvertEnumToTokenId(EnumNew);
            TypeOld = Parser.ConvertEnumToTokenId(EnumOld);
            TypeWhitelistRight = EnumWhitelistRight.Select(_enum => Parser.ConvertEnumToTokenId(_enum)).ToArray();
            TypeBlacklistRight = EenumBlacklistRight.Select(_enum => Parser.ConvertEnumToTokenId(_enum)).ToArray();
        }
        protected override void CustomDeinit()
        {
            TypeNew = 0;
            TypeOld = 0;
            TypeWhitelistRight = null;
            TypeBlacklistRight = null;
        }
        public override bool Process(ref int token)
        {
            int right = TokenManager.GetRight(token);
            if (right == 0)
            {
                return false;
            }
            ulong rightType = TokenManager.GetType(right);
            if ((TypeWhitelistRight.Length > 0 && !TypeWhitelistRight.Contains(rightType)) || (TypeBlacklistRight.Length > 0 && TypeBlacklistRight.Contains(rightType)))
            {
                return false;
            }

            TokenManager.Cut(right);
            TokenManager.PasteUnder(token, right);

            if (TypeNew >= 0)
            {
                TokenManager.SetType(token, TypeNew);
            }

            return true;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer UnaryLeft(this Syntaxer syntaxer, Enum token, Enum tokenMatched, params Enum[] tokenWhitelistRight)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorUnaryLeftWithFilter(token, tokenMatched, tokenWhitelistRight, null));
            return syntaxer;
        }
        public static Syntaxer UnaryLeft(this Syntaxer syntaxer, Enum token, Enum tokenMatched, IEnumerable<Enum> tokenWhitelistRight)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorUnaryLeftWithFilter(token, tokenMatched, tokenWhitelistRight, null));
            return syntaxer;
        }
        public static Syntaxer UnaryLeft(this Syntaxer syntaxer, Enum token, Enum tokenMatched, IEnumerable<Enum> tokenWhitelistRight, IEnumerable<Enum> tokenBlacklistRight)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorUnaryLeftWithFilter(token, tokenMatched, tokenWhitelistRight, tokenBlacklistRight));
            return syntaxer;
        }
    }
}
