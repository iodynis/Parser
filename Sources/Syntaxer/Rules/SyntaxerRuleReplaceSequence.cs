﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleReplaceSequence : SyntaxerRule
    {
        private int CountNew = 0;
        private int CountOld = 0;
        private List<Enum> EnumsNew = new List<Enum>();
        private List<ulong> TypesNew = null;
        private List<string> SymbolsNew = new List<string>();
        private List<Enum> EnumsOld = new List<Enum>();
        private List<ulong> TypesOld = null;
        private List<string> SymbolsOld = new List<string>();

        //public SyntaxerRuleReplaceSequence(Enum token, params Enum[] match)
        //    : this(token, null, match, null) { }
        public SyntaxerRuleReplaceSequence(IEnumerable<Enum> tokens, IEnumerable<Enum> tokensMatched)
        {
            if (tokensMatched.Count() == 0)
            {
                throw new Exception("Cannot replace an empty sequence. The sequence should contain at least one token.");
            }

            CountNew = tokens.Count();
            CountOld = tokensMatched.Count();
            EnumsNew.AddRange(tokens);
            SymbolsNew.AddRange(tokens.Select(_ => (string)null));
            EnumsOld.AddRange(tokensMatched);
            SymbolsOld.AddRange(tokensMatched.Select(_ => (string)null));

            Match(EnumsOld[0]);
        }
        public SyntaxerRuleReplaceSequence(IEnumerable<(Enum Token, string Symbol)> tokensAndSymbols, IEnumerable<(Enum Token, string Symbol)> tokensAndSymbolsMatched)
        {
            if (tokensAndSymbolsMatched.Count() == 0)
            {
                throw new Exception("Cannot replace an empty sequence. The sequence should contain at least one token.");
            }

            CountNew = tokensAndSymbols.Count();
            CountOld = tokensAndSymbolsMatched.Count();
            EnumsNew.AddRange(tokensAndSymbols.Select(_new => _new.Token));
            SymbolsNew.AddRange(tokensAndSymbols.Select(_new => _new.Symbol));
            EnumsOld.AddRange(tokensAndSymbolsMatched.Select(_old => _old.Token));
            SymbolsOld.AddRange(tokensAndSymbolsMatched.Select(_old => _old.Symbol));

            Match(EnumsOld[0]);
        }
        protected override void CustomInit()
        {
            TypesNew = new List<ulong>(EnumsNew.Select(_enum => Parser.ConvertEnumToTokenId(_enum)));
            TypesOld = new List<ulong>(EnumsOld.Select(_enum => Parser.ConvertEnumToTokenId(_enum)));
        }
        protected override void CustomDeinit()
        {
            TypesNew = null;
            TypesOld = null;
        }
        public override bool Process(ref int token)
        {
            int oldTokenLast = token;

            // Check tokens do match
            // And get the last token
            {
                int oldTokenCurrent = token;
                for (int i = 0; i < CountOld; i++)
                {
                    if (oldTokenCurrent == 0)
                    {
                        return false;
                    }
                    if (TypesOld[i] >= 0 && TypesOld[i] != TokenManager.GetType(oldTokenCurrent))
                    {
                        return false;
                    }
                    if (SymbolsOld[i] != null && SymbolsOld[i] != TokenManager.GetSymbol(oldTokenCurrent))
                    {
                        return false;
                    }
                    oldTokenLast = oldTokenCurrent;
                    oldTokenCurrent = TokenManager.GetRight(oldTokenCurrent);
                }
            }

            int start = TokenManager.GetTextStart(token);
            int stop = TokenManager.GetTextEnd(oldTokenLast);

            {
                int tokenCurrent = token;
                for (int i = 0; i < CountNew; i++)
                {
                    // Replace an existing token
                    if (i < CountOld)
                    {
                        TokenManager.Set(tokenCurrent, TypesNew[i], SymbolsNew[i], start, stop);
                        TokenManager.DeleteChildren(tokenCurrent);
                        tokenCurrent = TokenManager.GetRight(tokenCurrent);
                    }
                    // Add a new token
                    else
                    {
                        int tokenNew = TokenManager.Create(TypesNew[i], SymbolsNew[i], start, stop);
                        TokenManager.PasteRight(tokenCurrent, tokenNew);
                        tokenCurrent = tokenNew;
                    }
                }
                // Remove old tokens if any left
                if (CountNew < CountOld)
                {
                    TokenManager.DeleteBlock(TokenManager.GetRight(tokenCurrent), oldTokenLast);
                }
            }

            return true;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer ReplaceSequence(this Syntaxer syntaxer, IEnumerable<(Enum Token, string Symbol)> tokensAndSymbols, IEnumerable<(Enum Token, string Symbol)> tokensAndSymbolsMatched)
        {
            syntaxer.AddRule(new SyntaxerRuleReplaceSequence(tokensAndSymbols, tokensAndSymbolsMatched));
            return syntaxer;
        }
        public static Syntaxer ReplaceSequence(this Syntaxer syntaxer, IEnumerable<Enum> tokens, IEnumerable<Enum> tokensMatched)
        {
            syntaxer.AddRule(new SyntaxerRuleReplaceSequence(tokens, tokensMatched));
            return syntaxer;
        }
        public static Syntaxer ReplaceSequence(this Syntaxer syntaxer, (Enum Token, string Symbol) tokensAndSymbol, IEnumerable<(Enum Token, string Symbol)> tokensAndSymbolsMatched)
        {
            syntaxer.AddRule(new SyntaxerRuleReplaceSequence(new (Enum Token, string Symbol)[] { tokensAndSymbol }, tokensAndSymbolsMatched));
            return syntaxer;
        }
        public static Syntaxer ReplaceSequence(this Syntaxer syntaxer, Enum token, IEnumerable<Enum> tokensMatched)
        {
            syntaxer.AddRule(new SyntaxerRuleReplaceSequence(new Enum[] { token }, tokensMatched));
            return syntaxer;
        }
        public static Syntaxer ReplaceSequence(this Syntaxer syntaxer, Enum token, params Enum[] tokensMatched)
        {
            syntaxer.AddRule(new SyntaxerRuleReplaceSequence(new Enum[] { token }, tokensMatched));
            return syntaxer;
        }
    }
}
