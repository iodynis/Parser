﻿using System;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleGroupBlock : SyntaxerRule
    {
        private Enum EnumGroup;
        private ulong TypeGroup = 0;
        private string SymbolGroup;

        private Enum EnumOpen;
        private ulong TypeOpen = 0;
        private bool IncludeOpen;
        private Enum EnumClose;
        private ulong TypeClose = 0;
        private bool IncludeClose;

        public SyntaxerRuleGroupBlock(Enum token, Enum tokenMatchedOpening, Enum tokenMatchedClosing)
            : this(token, null, tokenMatchedOpening, tokenMatchedClosing) { }
        public SyntaxerRuleGroupBlock(Enum token, string symbol, Enum tokenMatchedOpening, Enum tokenMatchedClosing)
        {
            EnumGroup = token;
            SymbolGroup = symbol;
            EnumOpen = tokenMatchedOpening;
            IncludeOpen = false;
            EnumClose = tokenMatchedClosing;
            IncludeClose = false;

            Match(EnumOpen);
        }
        protected override void CustomInit()
        {
            TypeGroup = Parser.ConvertEnumToTokenId(EnumGroup);
            TypeOpen  = Parser.ConvertEnumToTokenId(EnumOpen);
            TypeClose = Parser.ConvertEnumToTokenId(EnumClose);
        }
        protected override void CustomDeinit()
        {
            TypeGroup = 0;
            TypeOpen = 0;
            TypeClose = 0;
        }
        public override bool Process(ref int token)
        {
            int depth = 1;
            int tokenCurrent = TokenManager.GetRight(token);
            while (tokenCurrent != 0)
            {
                ulong type = TokenManager.GetType(tokenCurrent);
                if (type == TypeOpen)
                {
                    depth++;
                }
                else if (type == TypeClose)
                {
                    depth--;
                }
                if (depth == 0)
                {
                    if (SymbolGroup != null)
                    {
                        token = TokenManager.Group(TypeGroup, SymbolGroup, token, IncludeOpen ? TokenManager.GroupMode.INCLUDE : TokenManager.GroupMode.DELETE, tokenCurrent, IncludeClose ? TokenManager.GroupMode.INCLUDE : TokenManager.GroupMode.DELETE);
                    }
                    else
                    {
                        token = TokenManager.Group(TypeGroup, token, IncludeOpen ? TokenManager.GroupMode.INCLUDE : TokenManager.GroupMode.DELETE, tokenCurrent, IncludeClose ? TokenManager.GroupMode.INCLUDE : TokenManager.GroupMode.DELETE);
                    }
                    return true;
                }
                tokenCurrent = TokenManager.GetRight(tokenCurrent);
            }
            var value = TokenManager.GetSymbol(token);
            var lineAndPosition = TokenManager.GetLineAndPosition(token);
            throw new Exception($"Symbol {value} of group {EnumGroup} on line {lineAndPosition.Line} at position {lineAndPosition.Position} is unbalanced.");
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer Group(this Syntaxer syntaxer, Enum typeGroup, Enum typeOpen, Enum typeClose)
        {
            syntaxer.AddRule(new SyntaxerRuleGroupBlock(typeGroup, typeOpen, typeClose));
            return syntaxer;
        }
        public static Syntaxer Group(this Syntaxer syntaxer, Enum typeGroup, string symbolGroup, Enum typeOpen, Enum typeClose)
        {
            syntaxer.AddRule(new SyntaxerRuleGroupBlock(typeGroup, symbolGroup, typeOpen, typeClose));
            return syntaxer;
        }
    }
}
