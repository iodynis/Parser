﻿using System;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleOperatorUnaryLeft : SyntaxerRule
    {
        private readonly Enum EnumNew;
        private ulong TypeNew = 0;
        private readonly Enum EnumOld;
        private ulong TypeOld = 0;

        public SyntaxerRuleOperatorUnaryLeft(Enum token)
            : this(token, token) { }
        public SyntaxerRuleOperatorUnaryLeft(Enum token, Enum tokenMatched)
            : base(tokenMatched)
        {
            EnumNew = token;
            EnumOld = tokenMatched;
        }
        protected override void CustomInit()
        {
            TypeNew = Parser.ConvertEnumToTokenId(EnumNew);
            TypeOld = MatchedTypes[0];
        }
        protected override void CustomDeinit()
        {
            TypeNew = 0;
            TypeOld = 0;
        }
        public override bool Process(ref int token)
        {
            int right = TokenManager.GetRight(token);
            if (right == 0)
            {
                return false;
            }

            TokenManager.Cut(right);
            TokenManager.PasteUnder(token, right);

            if (TypeNew >= 0)
            {
                TokenManager.SetType(token, TypeNew);
            }

            return true;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer UnaryLeft(this Syntaxer syntaxer, Enum token)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorUnaryLeft(token));
            return syntaxer;
        }
        public static Syntaxer UnaryLeft(this Syntaxer syntaxer, Enum token, Enum tokenMatched)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorUnaryLeft(token, tokenMatched));
            return syntaxer;
        }
    }
}
