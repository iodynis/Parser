﻿using System;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleRemove : SyntaxerRule
    {
        public SyntaxerRuleRemove(Enum token)
            : base(token)
        {
            ;
        }
        public override bool Process(ref int token)
        {
            int previous = TokenManager.GetPrevious(token);
            TokenManager.Delete(token);
            token = previous;
            return true;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer Remove(this Syntaxer syntaxer, Enum token)
        {
            syntaxer.AddRule(new SyntaxerRuleRemove(token));
            return syntaxer;
        }
    }
}
