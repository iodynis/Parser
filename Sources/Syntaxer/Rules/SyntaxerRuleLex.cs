﻿using System;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleLex : SyntaxerRule
    {
        private Lexer Lexer;

        public SyntaxerRuleLex(Lexer lexer)
        {
            Lexer = lexer;
        }
        public SyntaxerRuleLex(Enum token, Lexer lexer)
            : base(token)
        {
            Lexer = lexer;
        }
        protected override void CustomInit()
        {
            Lexer.Initialize(Parser);
        }
        protected override void CustomDeinit()
        {
            Lexer.Deinitialize();
        }
        public override bool Process(ref int token)
        {
            Lexer.Process(token);

            return true;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer Lex(this Syntaxer syntaxer, Lexer lexer)
        {
            syntaxer.AddRule(new SyntaxerRuleLex(lexer));
            return syntaxer;
        }
    }
}
