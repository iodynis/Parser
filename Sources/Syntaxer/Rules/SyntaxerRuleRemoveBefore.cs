﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleRemoveBefore : SyntaxerRule
    {
        private Enum[] TokensToRemove;
        private ulong[] TypesToRemove;

        public SyntaxerRuleRemoveBefore(Enum token, Enum tokenToRemove)
            : this(token, new Enum[] { tokenToRemove }) { }
        public SyntaxerRuleRemoveBefore(Enum token, IEnumerable<Enum> tokensToRemove)
            : this(token, tokensToRemove?.ToArray()) { }
        public SyntaxerRuleRemoveBefore(Enum token, params Enum[] tokensToRemove)
            : base(token)
        {
            if (tokensToRemove == null)
            {
                throw new ArgumentNullException(nameof(tokensToRemove));
            }
            TokensToRemove = tokensToRemove;
        }
        protected override void CustomInit()
        {
            TypesToRemove = TokensToRemove .Select(_enum => Parser.ConvertEnumToTokenId(_enum)).ToArray();
        }
        protected override void CustomDeinit()
        {
            TypesToRemove = null;
        }
        public override bool Process(ref int token)
        {
            int current = TokenManager.GetLeft(token);
            while (current != 0 && TypesToRemove.Contains(TokenManager.GetType(current)))
            {
                TokenManager.Delete(current);
            }
            return true;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer RemoveBefore(this Syntaxer syntaxer, Enum token, Enum tokenToRemove)
        {
            syntaxer.AddRule(new SyntaxerRuleRemoveBefore(token, tokenToRemove));
            return syntaxer;
        }
        public static Syntaxer RemoveBefore(this Syntaxer syntaxer, Enum token, IEnumerable<Enum> tokensToRemove)
        {
            syntaxer.AddRule(new SyntaxerRuleRemoveBefore(token, tokensToRemove));
            return syntaxer;
        }
    }
}
