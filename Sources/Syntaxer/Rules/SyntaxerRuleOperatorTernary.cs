﻿using System;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleOperatorTernary : SyntaxerRule
    {
        private readonly Enum EnumToken;
        private ulong TypeToken = 0;
        private readonly Enum EnumOperator;
        private ulong TypeOperator = 0;
        private readonly Enum EnumOperatorPartOne;
        private ulong TypeOperatorPartOne = 0;
        private readonly Enum EnumOperatorPartTwo;
        private ulong TypeOperatorPartTwo = 0;

        public SyntaxerRuleOperatorTernary(Enum tokenOperator, Enum tokenOperatorPartOne, Enum tokenOperatorPartTwo)
            : this(tokenOperator, tokenOperator, tokenOperatorPartOne, tokenOperatorPartTwo) { }
        public SyntaxerRuleOperatorTernary(Enum token, Enum tokenOperator, Enum tokenOperatorPartOne, Enum tokenOperatorPartTwo)
            : base(tokenOperatorPartOne)
        {
            EnumToken = token;
            EnumOperator = tokenOperator;
            EnumOperatorPartOne = tokenOperatorPartOne;
            EnumOperatorPartTwo = tokenOperatorPartTwo;
        }
        protected override void CustomInit()
        {
            TypeToken = Parser.ConvertEnumToTokenId(EnumToken);
            TypeOperator = Parser.ConvertEnumToTokenId(EnumOperator);
            TypeOperatorPartOne = MatchedTypes[0];
            TypeOperatorPartTwo = Parser.ConvertEnumToTokenId(EnumOperatorPartTwo);
        }
        protected override void CustomDeinit()
        {
            TypeToken = 0;
            TypeOperator = 0;
            TypeOperatorPartOne = 0;
            TypeOperatorPartTwo = 0;
        }
        public override bool Process(ref int token)
        {
            int leftOperand = TokenManager.GetLeft(token);
            if (leftOperand == 0)
            {
                return false;
            }
            int leftOperator = token;
            int middleOperand = TokenManager.GetRight(token);
            if (middleOperand == 0)
            {
                return false;
            }
            int rightOperator = TokenManager.GetRight(middleOperand);
            if (rightOperator == 0)
            {
                return false;
            }
            int rightOperand = TokenManager.GetRight(rightOperator);
            if (rightOperand == 0)
            {
                return false;
            }

            TokenManager.Delete(rightOperator);
            TokenManager.GroupAround(TypeOperator, TokenManager.GetSymbol(leftOperator) + TokenManager.GetSymbol(rightOperator), leftOperator, 1, 2);

            if (TypeToken >= 0)
            {
                TokenManager.SetType(token, TypeToken);
            }

            return true;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer Ternary(this Syntaxer syntaxer, Enum token, Enum tokenOperator, Enum tokenOperatorPartOne, Enum tokenOperatorPartTwo)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorTernary(token, tokenOperator, tokenOperatorPartOne, tokenOperatorPartTwo));
            return syntaxer;
        }
    }
}
