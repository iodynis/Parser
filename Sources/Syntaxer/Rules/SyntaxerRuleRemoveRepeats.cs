﻿using System;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleRemoveRepeats : SyntaxerRule
    {
        private Enum Enum;
        private ulong Type = 0;
        private int CountMaximum;

        public SyntaxerRuleRemoveRepeats(Enum token, int countMaximum = 1)
        {
            Enum = token;
            CountMaximum = countMaximum;

            Match(Enum);
        }
        protected override void CustomInit()
        {
            Type = Parser.ConvertEnumToTokenId(Enum);
        }
        protected override void CustomDeinit()
        {
            Type = 0;
        }
        public override bool Process(ref int token)
        {
            int current = TokenManager.GetNext(token);
            // Skip necessary count of tokens while the type matches
            int count = 1;
            while (count < CountMaximum)
            {
                current = TokenManager.GetNext(current);
                if (TokenManager.GetType(current) != Type)
                {
                    return false;
                }
                count++;
            }
            // Delete all following tokens while the type matches
            if (TokenManager.GetType(current) != Type)
            {
                while (true)
                {
                    int next = TokenManager.GetNext(current);
                    TokenManager.Delete(current);
                    current = next;
                    // Check the type still matches
                    if (TokenManager.GetType(current) != Type)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer RemoveRepeats(this Syntaxer syntaxer, Enum token, int countMaximum = 1)
        {
            syntaxer.AddRule(new SyntaxerRuleRemoveRepeats(token, countMaximum));
            return syntaxer;
        }
    }
}
