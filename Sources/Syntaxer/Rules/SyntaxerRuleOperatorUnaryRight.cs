﻿using System;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleOperatorUnaryRight : SyntaxerRule
    {
        private Enum EnumNew;
        private ulong TypeNew = 0;
        private Enum EnumOld;
        private ulong TypeOld = 0;

        public SyntaxerRuleOperatorUnaryRight(Enum token)
            : this(token, token) { }
        public SyntaxerRuleOperatorUnaryRight(Enum token, Enum tokenMatched)
        {
            EnumNew = token;
            EnumOld = tokenMatched;

            Match(EnumOld);
        }
        protected override void CustomInit()
        {
            TypeNew = Parser.ConvertEnumToTokenId(EnumNew);
            TypeOld = MatchedTypes[0];
        }
        protected override void CustomDeinit()
        {
            TypeNew = 0;
            TypeOld = 0;
        }
        public override bool Process(ref int token)
        {
            int left = TokenManager.GetLeft(token);
            if (left == 0)
            {
                return false;
            }

            TokenManager.Cut(left);
            TokenManager.PasteUnder(token, left);

            if (TypeNew >= 0)
            {
                TokenManager.SetType(token, TypeNew);
            }

            return true;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer UnaryRight(this Syntaxer syntaxer, Enum token)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorUnaryRight(token));
            return syntaxer;
        }
        public static Syntaxer UnaryRight(this Syntaxer syntaxer, Enum token, Enum tokenMatched)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorUnaryRight(token, tokenMatched));
            return syntaxer;
        }
    }
}
