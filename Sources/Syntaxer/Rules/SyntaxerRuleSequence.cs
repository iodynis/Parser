﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace Iodynis.Libraries.Parsing
//{
//    public class SyntaxerRuleSequence : SyntaxerRule
//    {
//        private Enum enumNew;
//        private long typeNew;

//        public SyntaxerRuleSequence(Enum token, params Enum[] matchedTokens)
//            : base(matchedTokens)
//        {
//            enumNew = token;
//        }
//        protected override void CustomInit()
//        {
//            typeNew = Parser.ConvertEnumToTokenId(enumNew);
//            //typesOld = MatchedTypes[0];
//        }
//        protected override void CustomDeinit()
//        {
//            typeNew = -1;
//            //typeOld = -1;
//        }
//        public override bool Process(ref int token)
//        {
//            int previous = TokenManager.GetPrevious(token);
//            TokenManager.Delete(token);
//            token = previous;
//            return true;
//        }
//    }
//    public static partial class SyntaxerExtensions
//    {
//        public static Syntaxer Sequence(this Syntaxer syntaxer, Enum token, params Enum[] matchedTokens)
//        {
//            syntaxer.AddRule(new SyntaxerRuleSequence(token), params Enum[] matchedTokens);
//            return syntaxer;
//        }
//    }
//}
