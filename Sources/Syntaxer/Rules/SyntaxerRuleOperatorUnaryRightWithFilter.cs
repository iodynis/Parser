﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleOperatorUnaryRightWithFilter : SyntaxerRule
    {
        private Enum EnumNew;
        private ulong TypeNew = 0;
        private Enum EnumOld;
        private ulong TypeOld = 0;
        private Enum[] EnumWhitelistLeft;
        private ulong[] TypeWhitelistLeft;
        private Enum[] EnumBlacklistLeft;
        private ulong[] TypeBlacklistLeft;

        public SyntaxerRuleOperatorUnaryRightWithFilter(Enum token, IEnumerable<Enum> tokenWhitelistLeft, IEnumerable<Enum> tokenBlacklistLeft)
            : this(token, token, tokenWhitelistLeft, tokenBlacklistLeft) { }
        public SyntaxerRuleOperatorUnaryRightWithFilter(Enum token, Enum tokenMatched, IEnumerable<Enum> tokenWhitelistLeft, IEnumerable<Enum> tokenBlacklistLeft)
            : base(tokenMatched)
        {
            EnumNew = token;
            EnumOld = tokenMatched;
            EnumWhitelistLeft = tokenWhitelistLeft?.ToArray() ?? new Enum[0];
            EnumBlacklistLeft = tokenBlacklistLeft?.ToArray() ?? new Enum[0];
        }
        protected override void CustomInit()
        {
            TypeNew = Parser.ConvertEnumToTokenId(EnumNew);
            TypeOld = Parser.ConvertEnumToTokenId(EnumOld);
            TypeWhitelistLeft = EnumWhitelistLeft.Select(_enum => Parser.ConvertEnumToTokenId(_enum)).ToArray();
            TypeBlacklistLeft = EnumBlacklistLeft.Select(_enum => Parser.ConvertEnumToTokenId(_enum)).ToArray();
        }
        protected override void CustomDeinit()
        {
            TypeNew = 0;
            TypeOld = 0;
            TypeWhitelistLeft = null;
            TypeBlacklistLeft = null;
        }
        public override bool Process(ref int token)
        {
            int left = TokenManager.GetLeft(token);
            if (left == 0)
            {
                return false;
            }
            ulong leftType = TokenManager.GetType(left);
            if ((TypeWhitelistLeft.Length > 0 && !TypeWhitelistLeft.Contains(leftType)) || (TypeBlacklistLeft.Length > 0 && TypeBlacklistLeft.Contains(leftType)))
            {
                return false;
            }

            TokenManager.Cut(left);
            TokenManager.PasteUnder(token, left);

            if (TypeNew >= 0)
            {
                TokenManager.SetType(token, TypeNew);
            }

            return true;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer UnaryRight(this Syntaxer syntaxer, Enum token, Enum tokenMatched, params Enum[] tokenWhitelistLeft)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorUnaryRightWithFilter(token, tokenMatched, tokenWhitelistLeft, null));
            return syntaxer;
        }
        public static Syntaxer UnaryRight(this Syntaxer syntaxer, Enum token, Enum tokenMatched, IEnumerable<Enum> tokenWhitelistLeft)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorUnaryRightWithFilter(token, tokenMatched, tokenWhitelistLeft, null));
            return syntaxer;
        }
        public static Syntaxer UnaryRight(this Syntaxer syntaxer, Enum token, Enum tokenMatched, IEnumerable<Enum> tokenWhitelistLeft, IEnumerable<Enum> tokenBlacklistLeft)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorUnaryRightWithFilter(token, tokenMatched, tokenWhitelistLeft, tokenBlacklistLeft));
            return syntaxer;
        }
    }
}
