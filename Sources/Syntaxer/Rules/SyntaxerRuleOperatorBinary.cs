﻿using System;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleOperatorBinary : SyntaxerRule
    {
        private readonly Enum EnumNew;
        private ulong TypeNew = 0;
        private readonly Enum EnumOld;
        private ulong TypeOld = 0;

        public SyntaxerRuleOperatorBinary(Enum token)
            : this(token, token) { }
        public SyntaxerRuleOperatorBinary(Enum token, Enum tokenMatched)
            : base(tokenMatched)
        {
            EnumNew = token;
            EnumOld = tokenMatched;
        }
        protected override void CustomInit()
        {
            TypeNew = Parser.ConvertEnumToTokenId(EnumNew);
            TypeOld = MatchedTypes[0];
        }
        protected override void CustomDeinit()
        {
            TypeNew = 0;
            TypeOld = 0;
        }
        public override bool Process(ref int token)
        {
            int left = TokenManager.GetLeft(token);
            if (left == 0)
            {
                return false;
            }
            int right = TokenManager.GetRight(token);
            if (right == 0)
            {
                return false;
            }

            TokenManager.Cut(left);
            TokenManager.Cut(right);

            TokenManager.PasteFirstUnder(token, left);
            TokenManager.PasteLastUnder(token, right);

            if (TypeNew >= 0)
            {
                TokenManager.SetType(token, TypeNew);
            }

            token = right;

            return true;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer Binary(this Syntaxer syntaxer, Enum token)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorBinary(token));
            return syntaxer;
        }
        public static Syntaxer Binary(this Syntaxer syntaxer, Enum token, Enum tokenMatched)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorBinary(token, tokenMatched));
            return syntaxer;
        }
    }
}
