﻿using System;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleSplit : SyntaxerRule
    {
        private Enum Token;
        private ulong Type = 0;
        private Enum GroupToken;
        private ulong GroupType = 0;
        private string GroupSymbol;
        private Enum DelimiterToken;
        private ulong DelimiterType = 0;

        public SyntaxerRuleSplit(Enum groupToken, Enum delimiterToken)
            : this(groupToken, (string)null, delimiterToken) { }
        public SyntaxerRuleSplit(Enum groupToken, string groupSymbol, Enum delimiterToken)
            : this(TokenBasic.ROOT, groupToken, groupSymbol, delimiterToken) { }
        //public SyntaxerRuleSplit(Enum groupToken, string groupSymbol, Enum delimiterToken)
        //    : base()
        //{
        //    this.groupToken     = groupToken;
        //    this.groupSymbol    = groupSymbol;
        //    this.delimiterToken = delimiterToken;
        //}
        public SyntaxerRuleSplit(Enum token, Enum groupToken, Enum delimiterToken)
            : this(token, groupToken, null, delimiterToken) { }
        public SyntaxerRuleSplit(Enum token, Enum groupToken, string groupSymbol, Enum delimiterToken)
            : base(token)
        {
            Token          = token;
            GroupToken     = groupToken;
            GroupSymbol    = groupSymbol;
            DelimiterToken = delimiterToken;
        }
        protected override void CustomInit()
        {
            Type          = MatchedTypes[0];
            GroupType     = Parser.ConvertEnumToTokenId(GroupToken);
            DelimiterType = Parser.ConvertEnumToTokenId(DelimiterToken);
        }
        protected override void CustomDeinit()
        {
            Type          = 0;
            GroupType     = 0;
            DelimiterType = 0;
        }
        public override bool Process(ref int token)
        {
            TokenManager.SplitChildren(token, GroupType, null, DelimiterType);

            return true;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer Split(this Syntaxer syntaxer, Enum token, Enum group, Enum delimiter)
        {
            syntaxer.AddRule(new SyntaxerRuleSplit(token, group, delimiter));
            return syntaxer;
        }
        public static Syntaxer Split(this Syntaxer syntaxer, Enum token, Enum group, string groupSymbol, Enum delimiter)
        {
            syntaxer.AddRule(new SyntaxerRuleSplit(token, group, groupSymbol, delimiter));
            return syntaxer;
        }
        public static Syntaxer Split(this Syntaxer syntaxer, Enum group, Enum delimiter)
        {
            syntaxer.AddRule(new SyntaxerRuleSplit(group, delimiter));
            return syntaxer;
        }
        public static Syntaxer Split(this Syntaxer syntaxer, Enum group, string groupSymbol, Enum delimiter)
        {
            syntaxer.AddRule(new SyntaxerRuleSplit(group, groupSymbol, delimiter));
            return syntaxer;
        }
    }
}
