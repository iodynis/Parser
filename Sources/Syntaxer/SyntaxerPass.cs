﻿using System;
using System.Collections.Generic;

namespace Iodynis.Libraries.Parsing
{
	public class SyntaxerPass
	{
        public string Name { get; }
		private List<SyntaxerRule> Rules { get; } = new List<SyntaxerRule>();
        private Dictionary<ulong, List<SyntaxerRule>> RulesForSpecificTokenType { get; } = new Dictionary<ulong, List<SyntaxerRule>>();
        private List<SyntaxerRule> RulesForAnyTokenType = new List<SyntaxerRule>();

        public Parser Parser { get; private set; }
        public Syntaxer Syntaxer { get; private set; }
        public TokenManager TokenManager { get; private set; }

        public SyntaxerPass()
            : this(null, null) { }
        public SyntaxerPass(string name)
            : this(name, null) { }
        public SyntaxerPass(IEnumerable<SyntaxerRule> rules)
            : this(null, rules) { }
        public SyntaxerPass(string name, IEnumerable<SyntaxerRule> rules)
        {
            Name = name;
            if (rules != null)
            {
                Rules.AddRange(rules);
            }
        }
        internal void Initialize(Syntaxer syntaxer)
        {
            // Set references
            Parser = syntaxer.Parser;
            Syntaxer = syntaxer;
            TokenManager = syntaxer.TokenManager;
            // Determine the rules to run against each token
            foreach (SyntaxerRule rule in Rules)
            {
                rule.Init(this);
                // The rule should be tried on every token
                if (rule.MatchedTypes.Count == 0)
                {
                    RulesForAnyTokenType.Add(rule);

                    foreach (ulong type in RulesForSpecificTokenType.Keys)
                    {
                        RulesForSpecificTokenType[type].Add(rule);
                    }
                }
                // The rule should be tried only on specific tokens
                else
                {
                    foreach (ulong type in rule.MatchedTypes)
                    {
                        if (!RulesForSpecificTokenType.TryGetValue(type, out List<SyntaxerRule> rules))
                        {
                            rules = new List<SyntaxerRule>();
                            // Add all-aplicable rules (this will preserve the rules order)
                            rules.AddRange(RulesForAnyTokenType);
                            // Add the new one
                            RulesForSpecificTokenType.Add(type, rules);
                        }
                        rules.Add(rule);
                    }
                }
            }
        }
        internal void Deinitialize()
        {
            RulesForSpecificTokenType.Clear();
            RulesForAnyTokenType.Clear();
            foreach (SyntaxerRule rule in Rules)
            {
                rule.Deinit();
            }
            TokenManager = null;
            Syntaxer = null;
            Parser = null;
        }
        /// <summary>
        /// Run the lexer pass over all root tokens.
        /// </summary>
        internal void Process()
        {
            List<int> roots = TokenManager.GetRoots();
            for (int rootIndex = 0; rootIndex < roots.Count; rootIndex++)
            {
                Process(roots[rootIndex]);
            }
        }
        internal void Process(int token)
        {
            int tokenCurrent = token;
            try
            {
                // Start a new pass inside the token manager
                TokenManager.StartPass();

                while (tokenCurrent != 0)
                {
                    // Mark the token as passed
                    TokenManager.SetPassed(tokenCurrent);

                    // Get rules for the token
                    ulong type = TokenManager.GetType(tokenCurrent);
                    if (!RulesForSpecificTokenType.TryGetValue(type, out List<SyntaxerRule> rules))
                    {
                        rules = RulesForAnyTokenType;
                    }
                    // Try all the rules sequentially till one finally succeeds or there are no more rules left
                    for (int ruleIndex = 0; ruleIndex < rules.Count; ruleIndex++)
                    {
                        SyntaxerRule rule = rules[ruleIndex];
                        if (rule.Process(ref tokenCurrent))
                        {
                            TokenManager.CheckConsistency();
                            // Only one rule of the pass can be applied at once
                            break;
                        }
                    }

                    // Move to the next yet unpassed token
                    tokenCurrent = TokenManager.GetNextUnpassed(tokenCurrent);
                }
            }
            catch (Exception exception)
            {
                (int line, int position) = TokenManager.GetLineAndPosition(tokenCurrent);
                string symbol = TokenManager.GetSymbol(tokenCurrent);
                ulong type = TokenManager.GetType(tokenCurrent);
                Enum tokenEnum = Parser.ConvertTokenIdToEnum(type);
                throw new SyntaxerException(line, position, symbol, tokenEnum, $"Failed to match line {line} at position {position} on pass {Name}.", exception);
            }
        }
        public SyntaxerPass AddRule(SyntaxerRule rule)
        {
            Rules.Add(rule);
            return this;
        }
	}
}
