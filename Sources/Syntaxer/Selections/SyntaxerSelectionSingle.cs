﻿using System;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerSelectionSingle : SyntaxerSelection
    {
        private Enum Token;
        private ulong Type = 0;

        public SyntaxerSelectionSingle(Enum token)
        {
            Token = token;
        }
        protected override void Init()
        {
            Type = Syntaxer.Parser.ConvertEnumToTokenId(Token);
        }
        public override void Deinit()
        {
            Type = 0;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static SyntaxerSelectionSingle Single(this Syntaxer syntaxer, Enum token)
        {
            SyntaxerSelectionSingle selection = new SyntaxerSelectionSingle(token);
            selection.Syntaxer = syntaxer;
            return selection;
        }
    }
}
