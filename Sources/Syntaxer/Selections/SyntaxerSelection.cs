﻿namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerSelection
    {
        protected TokenManager TokenManager { get; set; }
        internal Syntaxer Syntaxer { get; set; }

		protected SyntaxerSelection()
        {
            ;
        }
        public void Init(TokenManager tokenManager)
        {
            TokenManager = tokenManager;
            Init();
        }
        protected virtual void Init()
        {
            ;
        }
        public virtual void Deinit()
        {
            TokenManager = null;
        }
    }

    //public static partial class SyntaxerExtensions
    //{
    //    //public static SyntaxerSelection First(this Syntaxer syntaxer, Enum token)
    //    //{
    //    //    return new SyntaxerSelection();
    //    //    syntaxer.AddRule(new SyntaxerRuleReplace(token, match));
    //    //    return syntaxer;
    //    //}
    //}
}
