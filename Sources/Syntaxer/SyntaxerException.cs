﻿using System;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerException : Exception
    {
        public readonly int Line;
        public readonly int Position;
        public readonly string Symbol;
        public readonly Enum Token;

        public SyntaxerException(int line, int position, string symbol, Enum token)
            : this(line, position, symbol, token, null, null) { }
        public SyntaxerException(int line, int position, string symbol, Enum token, string message)
            : this(line, position, symbol, token, message, null) { }
        public SyntaxerException(int line, int position, string symbol, Enum token, Exception innerException)
            : this(line, position, symbol, token, null, innerException) { }
        public SyntaxerException(int line, int position, string symbol, Enum token, string message, Exception innerException)
            : base(message, innerException)
        {
            Line = line;
            Position = position;
            Symbol = symbol;
            Token = token;
        }
    }
}
