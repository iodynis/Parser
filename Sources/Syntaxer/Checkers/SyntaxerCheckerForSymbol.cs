﻿namespace Iodynis.Libraries.Parsing
{
	public class SyntaxerCheckerForSymbol : SyntaxerChecker
	{
		private string symbol;

		public SyntaxerCheckerForSymbol(string symbol)
        {
            this.symbol = symbol;
		}

		public override bool Check(int token)
		{
            return TokenManager.GetSymbol(token) == symbol;
		}
	}
}