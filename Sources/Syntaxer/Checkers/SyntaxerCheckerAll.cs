﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerCheckerAll : SyntaxerChecker
    {
        private readonly List<SyntaxerChecker> checkers = new List<SyntaxerChecker>();
        public SyntaxerCheckerAll(IEnumerable<SyntaxerChecker> checkers)
        {
            if (checkers == null)
            {
                throw new ArgumentNullException(nameof(checkers));
            }

            this.checkers.AddRange(checkers);
        }
		public override bool Check(int token)
		{
            return checkers.All(_checker => _checker.Check(token));
        }
    }
}
