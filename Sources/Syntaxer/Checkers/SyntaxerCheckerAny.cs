﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerCheckerAny : SyntaxerChecker
    {
        private readonly List<SyntaxerChecker> checkers = new List<SyntaxerChecker>();
        public SyntaxerCheckerAny(IEnumerable<SyntaxerChecker> checkers)
        {
            if (checkers == null)
            {
                throw new ArgumentNullException(nameof(checkers));
            }

            this.checkers.AddRange(checkers);
        }
		public override bool Check(int token)
		{
            return checkers.Any(_checker => _checker.Check(token));
        }
    }
}
