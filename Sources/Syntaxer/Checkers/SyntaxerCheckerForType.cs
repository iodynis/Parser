﻿using System;

namespace Iodynis.Libraries.Parsing
{
	public class SyntaxerCheckerForType : SyntaxerChecker
	{
		private ulong Type;

		public SyntaxerCheckerForType(Enum type)
        {
            Type = (ulong)(object)type;
		}

		public override bool Check(int token)
		{
            return TokenManager.GetType(token) == Type;
		}
	}
}
