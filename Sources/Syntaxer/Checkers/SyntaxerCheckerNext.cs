﻿using System;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerCheckerTypeNext : SyntaxerChecker
    {
		private ulong Type;

        public SyntaxerCheckerTypeNext(SyntaxerChecker checker, Enum type)
        {
            Type = (ulong)(object)type;
        }
		public override bool Check(int token)
		{
            return TokenManager.GetType(TokenManager.GetNext(token)) == Type;
		}
    }
}
