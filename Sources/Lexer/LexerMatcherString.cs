﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iodynis.Libraries.Parsing
{
    public class LexerMatcherString : LexerMatcher
    {
        private List<string> Strings { get; }

        public LexerMatcherString(Enum token, params string[] strings)
            : base(token)
        {
            if (strings == null)
            {
                throw new ArgumentNullException(nameof(strings));
            }
            if (strings.Any(_string => _string == null))
            {
                throw new ArgumentException("Provided strings contain a null string.", nameof(strings));
            }
            if (strings.Any(_string => _string.Length == 0))
            {
                throw new ArgumentException("Provided strings contain an empty string.", nameof(strings));
            }

            this.Strings = new List<string>(strings);
        }
        public override LexerMatch Match(string text, int index)
        {
            if (text == null)
            {
                throw new ArgumentNullException(nameof(text));
            }

            foreach (string @string in Strings)
            {
                // Skip if the string can't actually fit in
                if (index + @string.Length > text.Length)
                {
                    continue;
                }
                // Check if the opening sequence matches
                for (int position = 0; position < @string.Length; position++)
                {
                    // Skip if there are differencies
                    if (@string[position] != text[index + position])
                    {
                        goto next;
                    }
                }
                return new LexerMatch(TokenId, @string, index, index + @string.Length);
            next:;
            }
            return null;
        }
    }
    public static partial class LexerExtensions
    {
        public static Lexer String(this Lexer lexer, Enum token, params string[] strings)
        {
            lexer.AddMatcher(new LexerMatcherString(token, strings));
            return lexer;
        }
    }
}
