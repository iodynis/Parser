﻿namespace Iodynis.Libraries.Parsing
{
    public class LexerMatch
    {
        public readonly ulong TokenId;
        public readonly string String;
        public readonly int Start;
        public readonly int End;
        //public readonly LexerMatcher Matcher;

        //public LexerMatch(LexerMatcher matcher, string @string, int index, int length)
        public LexerMatch(ulong tokenId, string @string, int start, int end)
        {
            this.TokenId = tokenId;
            //Matcher = matcher;
            this.String = @string;
            this.Start = start;
            this.End = end;
        }
        public virtual void Add(int root, TokenManager tokenManager)
        {
            int tokenNew = tokenManager.Create(TokenId, String, Start, End);
            tokenManager.PasteLastUnder(root, tokenNew);
        }
    }
}
