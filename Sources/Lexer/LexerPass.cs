﻿using System;
using System.Collections.Generic;

namespace Iodynis.Libraries.Parsing
{
    public class LexerPass
    {
        public string Name { get; }
        private Enum Token { get; }
        private ulong Type { get; set; }
		private List<LexerMatcher> Matchers { get; } = new List<LexerMatcher>();
        public LexerPassMode Mode { get; }

        public Parser Parser { get; private set; }
        public Lexer Lexer { get; private set; }
        public TokenManager TokenManager { get; private set; }

        public LexerPass(Enum token)
            : this(token, null, null) { }
        public LexerPass(Enum token, LexerPassMode mode)
            : this(token, null, mode, null) { }
        public LexerPass(Enum token, string name)
            : this(token, name, null) { }
        public LexerPass(Enum token, string name, LexerPassMode mode)
            : this(token, name, mode, null) { }
        public LexerPass(Enum token, IEnumerable<LexerMatcher> matchers)
            : this(token, null, matchers) { }
        public LexerPass(Enum token, LexerPassMode mode, IEnumerable<LexerMatcher> matchers)
            : this(token, null, mode, matchers) { }
        public LexerPass(Enum token, string name, IEnumerable<LexerMatcher> matchers)
            : this(token, name, LexerPassMode.INHERIT, matchers) { }
        public LexerPass(Enum token, string name, LexerPassMode mode, IEnumerable<LexerMatcher> matchers)
        {
            Token = token;
            Name = name;
            Mode = mode;
            if (matchers != null)
            {
                Matchers.AddRange(matchers);
            }
        }
        internal void Initialize(Lexer lexer)
        {
            Parser = lexer.Parser;
            Lexer = lexer;
            TokenManager = lexer.TokenManager;
            Type = Parser.ConvertEnumToTokenId(Token);
            foreach (LexerMatcher matcher in Matchers)
            {
                matcher.Init(this);
            }
        }
        internal void Deinitialize()
        {
            foreach (LexerMatcher matcher in Matchers)
            {
                matcher.Deinit();
            }
            Type = 0;
            TokenManager = null;
            Lexer = null;
            Parser = null;
        }
        /// <summary>
        /// Run the lexer pass over all root tokens.
        /// </summary>
		internal void Process()
        {
            List<int> roots = TokenManager.GetRoots();
            for (int rootIndex = 0; rootIndex < roots.Count; rootIndex++)
            {
                int token = roots[rootIndex];
                //while (token != 0)
                //{
                    //int next = TokenManager.GetNext(token);
                    Process(token);
                    //token = next;
                //}
            }
        }
        /// <summary>
        /// Run the lexer pass over the specified token and its children.
        /// </summary>
        /// <param name="token">Token to run the lexer pass on.</param>
        internal void ProcessWithChildren(int token)
        {
            int down = TokenManager.GetDownFirst(token);
            int right = TokenManager.GetRight(token);

            // Skip the token processing and go down
            if (down != 0)
            {
                ProcessWithChildren(down);
            }
            // Process the token
            else
            {
                Process(token);
            }

            // Go right
            if (right != 0)
            {
                ProcessWithChildren(right);
            }
        }
        /// <summary>
        /// Run the lexer pass over the specified token.
        /// </summary>
        /// <param name="token">Token to run the lexer pass on.</param>
        internal void Process(int token)
        {
            if (Type != TokenManager.GetType(token))
            {
                return;
            }

            string text = TokenManager.GetSymbol(token);

			// Index indicates current position in the string
			int index = 0;
			// This buffer will keep intermediate token text
			string buffer = "";
            LexerPassMode mode = Mode == LexerPassMode.INHERIT ? Lexer.Mode.ToLexerPassMode() : Mode;

            try
            {
			    while (index < text.Length)
			    {
                    LexerMatch match = null;
                    LexerMatcher matcher = null;

				    // Check the rules
				    for (int matcherIndex = 0; matcherIndex < Matchers.Count; matcherIndex++)
				    {
                        LexerMatcher matcherTry  = Matchers[matcherIndex];
                        LexerMatch matchTry = matcherTry.Match(text, index);
                        if (matchTry != null)
                        {
                            // One match is enough
                            if (mode == LexerPassMode.MATCH_FIRST)
                            {
                                match = matchTry;
                                matcher = matcherTry;
                                break;
                            }
                            // Need to go through all matches
                            else
                            {
                                // Matched first
                                if (match == null)
                                {
                                    match = matchTry;
                                    matcher = matcherTry;
                                }
                                // Matched a new one and it is better than the previously stored one
                                else if ((matchTry.End > match.End && mode == LexerPassMode.MATCH_LONGEST) ||
                                    (matchTry.End < match.End && mode == LexerPassMode.MATCH_SHORTEST))
                                {
                                    match = matchTry;
                                    matcher = matcherTry;
                                }
                            }
                        }
				    }
				    // If no match was found - so append to buffer, from which an unknown token will be created at last
				    if (match == null)
				    {
					    buffer += text[index];
					    index++;
                        continue;
                    }
				    // If buffer is not empty - create an unknown token and reset the buffer
				    if (buffer != "")
				    {
                        Token tokenUnknown = new Token(-1, buffer, index - buffer.Length, index);
                        TokenManager.UnknownTokens.Add(tokenUnknown);
				        buffer = "";
				    }
				    // Get a token from the match
                    match.Add(token, TokenManager);
				    index = match.End;
			    }
                // If buffer is not empty - create an unknown token
                if (buffer != "")
                {
                    Token tokenUnknown = new Token(-1, buffer, index - buffer.Length, index);
                    TokenManager.UnknownTokens.Add(tokenUnknown);
                }
            }
            catch (Exception exception)
            {
                (int line, int position) = TokenManager.GetLineAndPosition(token, index);
                throw new LexerException(line, position, $"Failed to match line {line} at position {position}.", exception);
            }
		}
        public LexerPass AddMatcher(LexerMatcher matcher)
        {
            Matchers.Add(matcher);
            return this;
        }
    }
}
