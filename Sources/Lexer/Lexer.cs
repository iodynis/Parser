﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iodynis.Libraries.Parsing
{
    /// <summary>
    /// Lexer transforms a text string into a list of tokens.
    /// </summary>
	public class Lexer
	{
        internal bool Debug { get; private set; }
        public LexerMode Mode { get; }
		internal List<LexerPass> Passes { get; } = new List<LexerPass>();

        public Parser Parser { get; private set; }
        public TokenManager TokenManager => Parser.TokenManager;

		public Lexer()
            : this(null) { }
		public Lexer(LexerMode mode)
            : this(mode, null) { }
		public Lexer(IEnumerable<LexerPass> passes)
            : this(LexerMode.MATCH_LONGEST, passes) { }
		public Lexer(LexerMode mode, IEnumerable<LexerPass> passes)
        {
            //if (mode == LexerMode.INHERIT)
            //{
            //    throw new ArgumentException($"Lexer mode for the lexer itself can be either {LexerMode.MATCH_FIRST}, {LexerMode.MATCH_LONGEST} or {LexerMode.MATCH_SHORTEST}. It cannot be {LexerMode.INHERIT}. {LexerMode.INHERIT} mode can be used in a lexer pass.", nameof(mode));
            //}
            Mode = mode;
            if (passes != null)
            {
                Passes.AddRange(passes);
            }
        }
        internal void Initialize(Parser parser)
        {
            if (parser == null)
            {
                throw new ArgumentNullException(nameof(parser));
            }

            Debug = parser.Debug;
            Parser = parser;
            //TokenManager = parser.TokenManager;
            foreach (LexerPass pass in Passes)
            {
                pass.Initialize(this);
            }
        }
        internal void Deinitialize()
        {
            foreach (LexerPass pass in Passes)
            {
                pass.Deinitialize();
            }
            //TokenManager = null;
            Parser = null;
            Debug = false;
        }
        /// <summary>
        /// Run the lexer over the specified token.
        /// </summary>
        /// <param name="token">Token to run the lexer on.</param>
		internal void Process(int token)
		{
            foreach (LexerPass pass in Passes)
            {
				pass.Process(token);
            }
		}
        /// <summary>
        /// Run the lexer over all root tokens.
        /// </summary>
		internal void Process()
		{
            for (int passIndex = 0; passIndex < Passes.Count; passIndex++)
            {
                LexerPass pass = Passes[passIndex];

                try
                {
                    if (Debug)
                    {
                        Parser.OnDebugEvent(new ParserDebugEventArguments(ParserDebugEventArgumentStageType.LEXER_PASS_BEFORE, passIndex, pass.Name));
                    }

				    pass.Process();

                    if (Debug)
                    {
                        Parser.OnDebugEvent(new ParserDebugEventArguments(ParserDebugEventArgumentStageType.LEXER_PASS_AFTER_SUCCESS, passIndex, pass.Name));
                    }
                }
                catch
                {
                    if (Debug)
                    {
                        Parser.OnDebugEvent(new ParserDebugEventArguments(ParserDebugEventArgumentStageType.LEXER_PASS_AFTER_ERROR, passIndex, pass.Name));
                    }
                    throw;
                }
            }
		}
        /// <summary>
        /// Add a pass.
        /// </summary>
        /// <param name="token">Token over which the pass will take place.</param>
        /// <returns>Itself for chaining.</returns>
        public Lexer AddPass(Enum token)
        {
            return AddPass(token, null);
        }
        /// <summary>
        /// Add a pass.
        /// </summary>
        /// <param name="token">Token over which the pass will take place.</param>
        /// <param name="name">Custom name of the pass.</param>
        /// <returns>Itself for chaining.</returns>
        public Lexer AddPass(Enum token, string name)
        {
            return AddPass(new LexerPass(token, name));
        }
        /// <summary>
        /// Add a pass.
        /// </summary>
        /// <param name="pass">Pass.</param>
        /// <returns>Itself for chaining.</returns>
        public Lexer AddPass(LexerPass pass)
        {
            Passes.Add(pass);
            return this;
        }
        /// <summary>
        /// Add a matcher to the last pass added.
        /// </summary>
        /// <param name="matcher">Pass.</param>
        /// <returns>Itself for chaining.</returns>
        public Lexer AddMatcher(LexerMatcher matcher)
        {
            if (Passes.Count == 0)
            {
                AddPass(TokenBasic.ROOT);
            }
            Passes.Last().AddMatcher(matcher);
            return this;
        }
	}
}
