﻿using System;

namespace Iodynis.Libraries.Parsing
{
    /// <summary>
    /// Lexer pass mode defines how to resolve multiple matches.
    /// </summary>
    public enum LexerPassMode
    {
        /// <summary>
        /// Use the same settings as the superior class uses. This is the default behavior for a lexer pass.
        /// </summary>
        INHERIT,
        /// <summary>
        /// Use the very first successful match.
        /// </summary>
        MATCH_FIRST,
        /// <summary>
        /// In case of multiple tokens matched use the shortest one.
        /// </summary>
        MATCH_SHORTEST,
        /// <summary>
        /// In case of multiple tokens matched use the longest one. This is the default behavior for a lexer.
        /// </summary>
        MATCH_LONGEST,
    }
    public static partial class Extensions
    {
        public static LexerMode ToLexerMode(this LexerPassMode mode)
        {
            switch (mode)
            {
                case LexerPassMode.MATCH_FIRST:
                    return LexerMode.MATCH_FIRST;
                case LexerPassMode.MATCH_LONGEST:
                    return LexerMode.MATCH_LONGEST;
                case LexerPassMode.MATCH_SHORTEST:
                    return LexerMode.MATCH_SHORTEST;
                default:
                    throw new InvalidCastException($"Lexer pass mode of {mode} cannot be cast to lexer mode.");
            }
        }
    }
}
