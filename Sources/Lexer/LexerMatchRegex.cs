﻿namespace Iodynis.Libraries.Parsing
{
    public class LexerMatchRegex : LexerMatch
    {
        public readonly ulong[] TokenIds;
        public readonly string[] Strings;
        public readonly int[] Starts;
        public readonly int[] Ends;
        public LexerMatchRegex(ulong tokenId, string @string, int start, int end, ulong[] tokenIds, string[] strings, int[] starts, int[] ends)
            : base(tokenId, @string, start, end)
        {
            TokenIds = tokenIds;
            Strings = strings;
            Starts = starts;
            Ends = ends;
        }
        public LexerMatchRegex(ulong tokenId, string @string, int start, int end)
            : base(tokenId, @string, start, end)
        {
            ;
        }
        public override void Add(int root, TokenManager tokenManager)
        {
            int tokenRoot = tokenManager.Create(TokenId, String, Start, End);
            tokenManager.PasteLastUnder(root, tokenRoot);

            if (TokenIds != null)
            {
                for (int i = 0; i < TokenIds.Length; i++)
                {
                    int tokenChild = tokenManager.Create(TokenIds[i], Strings[i], Starts[i], Ends[i]);
                    tokenManager.PasteLastUnder(tokenRoot, tokenChild);
                }
            }
        }
    }
}
