﻿using System;

namespace Iodynis.Libraries.Parsing
{
    public abstract class LexerMatcher
    {
        public Enum TokenEnum { get; }
        public ulong TokenId { get; private set; }

        public Parser Parser { get; private set; }
        public Lexer Lexer { get; private set; }
        public LexerPass LexerPass { get; set; }
        public TokenManager TokenManager { get; private set; }

        protected LexerMatcher(Enum token)
        {
            if (token == null)
            {
                throw new ArgumentNullException(nameof(token));
            }

            TokenEnum = token;
        }
        internal void Init(LexerPass lexerPass)
        {
            // Set references
            Parser = lexerPass.Parser;
            Lexer = lexerPass.Lexer;
            LexerPass = lexerPass;
            TokenManager = lexerPass.TokenManager;
            TokenId = Parser.ConvertEnumToTokenId(TokenEnum);

            // Custom
            CustomInit();
        }
        protected virtual void CustomInit()
        {
            ;
        }
        internal void Deinit()
        {
            // Custom
            CustomDeinit();

            TokenId = 0;
            // Clear references
            TokenManager = null;
            Lexer = null;
            LexerPass = null;
            Parser = null;
        }
        protected virtual void CustomDeinit()
        {
            ;
        }
        public virtual LexerMatch Match(string text, int index)
        {
            throw new NotImplementedException();
        }
    }
}
