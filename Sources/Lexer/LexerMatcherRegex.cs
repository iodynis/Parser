﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Iodynis.Libraries.Parsing
{
    public class LexerMatcherRegex : LexerMatcher
    {
        public enum MatcherMode
        {
            WALK_PREMATCH_MATCH,
            WALK_PREMATCH_MATCH_POSTMATCH,
        }
        private MatcherMode Mode { get; }
        private Regex[] Regexes { get; }
        private Enum[] TokenEnums { get; }
        public ulong[] TokenIds { get; private set; }

        public LexerMatcherRegex(Enum token, params string[] regexes)
            : this(token, MatcherMode.WALK_PREMATCH_MATCH_POSTMATCH, regexes) { }
        public LexerMatcherRegex(Enum token, IEnumerable<string> regexes)
            : this(token, MatcherMode.WALK_PREMATCH_MATCH_POSTMATCH, regexes) { }
        public LexerMatcherRegex(Enum token, IEnumerable<string> regexes, params Enum[] tokens)
            : this(token, MatcherMode.WALK_PREMATCH_MATCH_POSTMATCH, regexes, tokens) { }

        public LexerMatcherRegex(Enum token, string regex, MatcherMode mode)
            : this(token, mode, new string[] { regex }, new Enum[0]) { }
        /// <summary>
        /// Match a regex.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="mode">Mode defines whether to </param>
        /// <param name="regexes">Regexes will compile with a \G key. In case you need to use groups besides the matching one -- use unnamed groups (?: ... ).</param>
        /// <param name="tokens"></param>
        private LexerMatcherRegex(Enum token, MatcherMode mode, IEnumerable<string> regexes, params Enum[] tokens)
            : base(token)
        {
            if (regexes == null)
            {
                throw new ArgumentNullException(nameof(regexes));
            }
            if (tokens == null)
            {
                throw new ArgumentNullException(nameof(tokens));
            }

            Mode = mode;
            Regexes = regexes.Select(_string => new Regex(@"\G" + _string, RegexOptions.Compiled)).ToArray();
            //Regexes = regexes.Select(_string => new Regex(_string)).ToArray();
            TokenEnums = tokens.ToArray();
        }

        public LexerMatcherRegex(Enum token, params Regex[] regexes)
            : this(token, MatcherMode.WALK_PREMATCH_MATCH_POSTMATCH, regexes) { }
        public LexerMatcherRegex(Enum token, IEnumerable<Regex> regexes)
            : this(token, MatcherMode.WALK_PREMATCH_MATCH_POSTMATCH, regexes) { }
        public LexerMatcherRegex(Enum token, IEnumerable<Regex> regexes, params Enum[] tokens)
            : this(token, MatcherMode.WALK_PREMATCH_MATCH_POSTMATCH, regexes, tokens) { }
        public LexerMatcherRegex(Enum token, Regex regex, MatcherMode mode)
            : this(token, mode, new Regex[] { regex }, new Enum[0]) { }

        private LexerMatcherRegex(Enum token, MatcherMode mode, IEnumerable<Regex> regexes, params Enum[] tokens)
            : base(token)
        {
            if (regexes == null)
            {
                throw new ArgumentNullException(nameof(regexes));
            }
            if (tokens == null)
            {
                throw new ArgumentNullException(nameof(tokens));
            }

            Mode = mode;
            Regexes = regexes.ToArray();
            TokenEnums = tokens.ToArray();
        }
        protected override void CustomInit()
        {
            TokenIds = TokenEnums.Select(_tokenEnum => Parser.ConvertEnumToTokenId(_tokenEnum)).ToArray();
        }
        protected override void CustomDeinit()
        {
            TokenIds = null;
        }
        public override LexerMatch Match(string text, int index)
        {
            if (text == null)
            {
                throw new ArgumentNullException(nameof(text));
            }

            foreach (Regex regex in Regexes)
            {
                Match match = regex.Match(text, index);
                if (!match.Success)
                {
                    continue;
                }
                // In case of a single token
                if (TokenIds.Length == 0)
                {
                    if (match.Groups.Count == 1)
                    {
                        //return new LexerMatchRegex(TokenId, match.Value, index, match.Value.Length);
                        return new LexerMatchRegex(TokenId, match.Value, match.Index, match.Index + match.Length);
                    }
                    else if (match.Groups.Count == 2)
                    {
                        if (Mode == MatcherMode.WALK_PREMATCH_MATCH)
                        {
                            int start = match.Groups[1].Index;
                            int end = start + match.Groups[1].Length;
                            return new LexerMatchRegex(TokenId, match.Groups[1].Value, start, end);
                        }
                        else // Mode == MatcherMode.WALK_PREMATCH_MATCH_POSTMATCH
                        {
                            int start = match.Groups[1].Index;
                            int end = start + match.Groups[1].Length;
                            return new LexerMatchRegex(TokenId, match.Groups[1].Value, start, end);
                        }
                    }
                    else
                    {
                        throw new Exception($"Regex matcher {regex} captured {match.Groups.Count - 1} groups while only 0 or 1 group is supported besides the whole match itself.");
                    }
                }
                // In case of multiple tokens
                else
                {
                    int count = match.Groups.Count - 1 /* First group is the whole match */;
                    string[] values = new string[count];
                    int[] starts = new int[count];
                    int[] ends = new int[count];
                    for (int i = 0; i < count; i++)
                    {
                        int groupIndex = i + 1 /* First group is the whole match */;
                        values[i] = match.Groups[groupIndex].Value;
                        starts[i] = match.Groups[groupIndex].Index;
                        ends[i] = starts[i] + match.Groups[groupIndex].Length;
                    }
                    return new LexerMatchRegex(TokenId, match.Value, index, index + match.Length, TokenIds, values, starts, ends);
                }
            }
            return null;
        }
    }
    public static partial class LexerExtensions
    {
        public static Lexer Regex(this Lexer lexer, Enum token, params string[] regexes)
        {
            lexer.AddMatcher(new LexerMatcherRegex(token, regexes));
            return lexer;
        }
        public static Lexer Regex(this Lexer lexer, Enum token, string regex, LexerMatcherRegex.MatcherMode mode)
        {
            lexer.AddMatcher(new LexerMatcherRegex(token, regex, mode));
            return lexer;
        }
        public static Lexer Regex(this Lexer lexer, Enum token, string regex, params Enum[] tokens)
        {
            lexer.AddMatcher(new LexerMatcherRegex(token, new string[] { regex }, tokens));
            return lexer;
        }
    }
}
