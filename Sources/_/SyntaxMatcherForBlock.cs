﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace Iodynis.Libraries.Parsing
//{
//	public class SyntaxMatcherForBlock : SyntaxerMatcher
//	{
//		private SyntaxerMatcher matcherStart;
//		private SyntaxerMatcher matcherStop;
//		private uint nestingDepthMaximum;
//		//private bool nestingAllowed;

//		private SyntaxMatcherForBlock(SyntaxerMatcher matcherStart, SyntaxerMatcher matcherStop, uint nestingDepthMaximum)
//			: base()
//		{
//			this.matcherStart  = matcherStart;
//			this.matcherStop = matcherStop;
//			this.nestingDepthMaximum = nestingDepthMaximum;
//		}

//		public override bool Match(Token param_token, out SyntaxerMatch match)
//		{
//			return Match(param_token, nestingDepthMaximum, out match);
//		}
//		private bool Match(Token token, bool nesting, out SyntaxerMatch match)
//		{
//			match = null;
//			if (token == null)
//			{
//				return false;
//			}

//            // Match start
//			if (!matcherStart.Match(token, out _))
//            {
//                return false;
//            }

//		    int counter = 0;
//		    int depth = 1;
//		    Token tokenCurrent = token.Right;
//			while (tokenCurrent != null)
//			{
//                // Match end
//				if (matcherStop.Match(tokenCurrent, out _))
//				{
//                    depth--;
//                    // Emerge
//					if (depth == 0)
//					{

//			            match = new SyntaxerMatch(this, new List<Token> { token, tokenCurrent });//matchStart.Count + matchStop.Count + skippedTokensCount);
//                        return true;
//					}
//				}
//                // Match internal start if nesting is supported
//				else if (nesting)
//				{
//					if (matcherStart.Match(tokenCurrent, out _))
//					{
//						depth++;
//					}
//				}
//				tokenCurrent = tokenCurrent.Right;
//				counter++;
//			}
//            return false;
//		}
//	}
//}
