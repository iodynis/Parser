﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Text.RegularExpressions;

//namespace Iodynis.Libraries.Parsing
//{
//	public class LexemeRule : Rule
//	{
//		//private readonly LexemeMatcher field_matcher;
//		public LexemeMatcher Matcher { get; }
//		//private readonly LexemeOperator field_operator;
//		//public LexemeOperator Operator { get { return field_operator; } }

//		public LexemeRule(LexemeMatcher matcher, int priority = 0)
//			: base(priority)
//		{
//			Matcher = matcher;
//			Matcher.Rule = this;
//			//field_operator = param_operator;
//			//field_operator.Rule = this;
//		}

//		public static bool operator == (LexemeRule param_lexemeRuleLeft, LexemeRule param_lexemeRuleRight)
//		{
//			if (ReferenceEquals(param_lexemeRuleLeft, null))
//			{
//				return ReferenceEquals(param_lexemeRuleRight, null);
//			}
//			if (ReferenceEquals(param_lexemeRuleRight, null))
//			{
//				return ReferenceEquals(param_lexemeRuleLeft, null);
//			}

//			return param_lexemeRuleLeft.GetType() == param_lexemeRuleRight.GetType() && param_lexemeRuleLeft.ToString() == param_lexemeRuleRight.ToString();
//		}
//		public static bool operator != (LexemeRule param_lexemeRuleLeft, LexemeRule param_lexemeRuleRight)
//		{
//			if (ReferenceEquals(param_lexemeRuleLeft, null))
//			{
//				return !ReferenceEquals(param_lexemeRuleRight, null);
//			}
//			if (ReferenceEquals(param_lexemeRuleRight, null))
//			{
//				return !ReferenceEquals(param_lexemeRuleLeft, null);
//			}

//			return param_lexemeRuleLeft.GetType() != param_lexemeRuleRight.GetType() || param_lexemeRuleLeft.ToString() != param_lexemeRuleRight.ToString();
//		}
//		public override bool Equals(object param_object)
//		{
//			return param_object.GetType() == typeof(LexemeRule) && this == (LexemeRule)param_object;
//		}
//		//public override int GetHashCode()
//		//{
//		//	return (base.GetHashCode() << 8) + (field_matcher.GetHashCode() << 4) + field_operator.GetHashCode();
//		//}
//		//public override string ToString()
//		//{
//		//	return field_matcher != null ? field_matcher.ToString() : "";
//		//}
//	}
//}
