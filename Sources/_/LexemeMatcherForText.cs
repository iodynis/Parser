﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Text.RegularExpressions;

//namespace Iodynis.Libraries.Parsing
//{
//	public class LexemeMatcherForText: LexemeMatcher
//	{
//		public string Symbol { get; }

//		public LexemeMatcherForText(string symbol)
//			: base()
//		{
//			if (String.IsNullOrEmpty(symbol))
//			{
//				throw new Exception("Symbol cannot be null or empty.");
//			}
//			Symbol = symbol;
//		}

//		public override bool Match(string text, int index, out LexemeMatch match)
//		{
//            match = null;

//			if (text == null)
//			{
//				return false;
//			}

//			if (index + Symbol.Length <= text.Length && Symbol == text.Substring(index, Symbol.Length))
//			{
//				match = new LexemeMatch(this, Symbol, index, index + Symbol.Length);
//			    return true;
//			}

//			return false;
//		}

//		public override string ToString()
//		{
//			return Symbol;
//		}
//	}
//}
