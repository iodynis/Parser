﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Text.RegularExpressions;
//using System.Threading.Tasks;

//namespace Iodynis.Libraries.Parsing
//{
//    public class LexerMatcherRegexMultiple : LexerMatcher
//    {
//        private List<Regex> Regexes { get; }

//        public LexerMatcherRegexMultiple(Enum token, string @string, Enum token)
//            : base(token)
//        {
//            if (@string == null)
//            {
//                throw new ArgumentNullException(nameof(@string));
//            }

//            Regexes = list new Regex(@"\G" + _string)).ToList();
//        }
//        public LexerMatcherRegexMultiple(Enum token, params Regex[] regexes)
//            : this(token, regexes as IEnumerable<Regex>) { }
//        public LexerMatcherRegexMultiple(Enum token, IEnumerable<Regex> regexes)
//            : base(token)
//        {
//            if (regexes == null)
//            {
//                throw new ArgumentNullException(nameof(regexes));
//            }

//            Regexes = new List<Regex>(regexes);
//        }
//        public override LexerMatch Match(string text, int index)
//        {
//            if (text == null)
//            {
//                throw new ArgumentNullException(nameof(text));
//            }

//            foreach (Regex regex in Regexes)
//            {
//                Match match = regex.Match(text, index);
//                if (!match.Success)
//                {
//                    continue;
//                }
//                return new LexerMatch(this, match.Value, index, match.Value.Length);
//            }
//            return null;
//        }
//    }
//    public static partial class LexerExtensions
//    {
//        public static Lexer Regex(this Lexer lexer, Enum token, params string[] regexes)
//        {
//            lexer.AddMatcher(new LexerMatcherRegexSingle(token, regexes));
//            return lexer;
//        }
//    }
//}
