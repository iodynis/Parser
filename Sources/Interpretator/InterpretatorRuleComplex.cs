﻿using System;

namespace Iodynis.Libraries.Parsing
{
    public class InterpretatorRuleComplex : InterpretatorRule
    {
        private Func<TokenManager, int, object> Function { get; }
        public InterpretatorRuleComplex(Enum token, Func<TokenManager, int, object> function)
        {
            Function = function;
            Match(token);
        }
        protected override void CustomInit()
        {
            ;
        }
        protected override void CustomDeinit()
        {
            ;
        }
        public override bool Process(int token)
        {
            object @object = Function.Invoke(TokenManager, token);
            TokenManager.SetObject(token, @object);

            return true;
        }
    }
    public static partial class InterpretatorExtensions
    {
        public static Interpretator Complex(this Interpretator interpretator, Enum token, Func<TokenManager, int, object> function)
        {
            interpretator.AddRule(new InterpretatorRuleComplex(token, function));
            return interpretator;
        }
    }
}
